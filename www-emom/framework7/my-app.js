//var ipserver = "http://119.252.168.10:388/E-MOM/e_mom_server/";
var ipserver = "http://10.1.13.54/E-MOM/e_mom_server/";



var ls_id = "", ls_nama = "", ls_email = "", ls_pass = "";
var lokasi_sekrng = "index";

/*global Framework7, Dom7 */

var myApp = new Framework7({
    material: true,
    tapHold: true,
    tapHoldDelay: 1000,
    swipePanelOnlyClose: true,
    swipeoutNoFollow: true,
    swipePanelNoFollow: true,
    // hideNavbarOnPageScroll: true,
    //materialRipple: false,
    materialRippleElements: '.ripple, a.link, .button, .modal-button, .tab-link, .label-radio, .label-checkbox, .actions-modal-button, a.searchbar-clear, .floating-button'
});
// Export selectors engine
var $$ = Dom7;
// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    // dynamicNavbar: true
});
$$(document).on("click", ".tes_notif", function () {

    var navigasi = "pic";
    myApp.addNotification({
        message: '<span class="color-yellow">Judul Notificatoin</span><br/>'
                + 'Topic meeting dicoba teks panjang sekali lagi<br/>'
                + '<span class="color-lightgreen">2016-11-03</span>',
        hold: 5000,
        closeOnClick: true,
        onClick: function () {
            if (navigasi == "home") {
                mainView.router.loadPage("index.html");
            } else if (navigasi == "lead") {
                mainView.router.loadPage("lead.html");
            } else if (navigasi == "pic") {
                mainView.router.loadPage("pic.html");
            }
            myApp.closePanel();
        }
    });
});
/* ===== Modals Page end index  ===== */

//api key
var mySearchbar = $$('.searchbar')[0].f7Searchbar;
app.initialize();
function update_apikey() {
    // var eps=localStorage.getItem("id_eps");

    var eps = $$("#api_key").val();
    //alert(ls_id + " & " + eps);

    if (eps == "" || eps == null) {
        //alert("kosong ipkeynya");  
        app.initialize();
        setTimeout(function () {
            update_apikey();
        }, 5000);
    } else {

        $$.post(ipserver + "update_apikey.php", {ep: ls_id, api: eps}, function (data) {
            var arr = $.parseJSON(data);
            //var arr = (data);
            if (arr.status == "sukses") {
                //alert('API Key berhasil diperbaharui');
            } else {
                alert("API Key gagal diperbaharui");
            }
        });
    }

}
;
//validate login
$("#f_login").validate({
    rules: {
        email: {
            required: true
        },
        password: {
            required: true
        }

    }
});
//end validate login
var awal = 0;
$$(document).on("click", ".b_home", function () {
//alert("home"); 
    lokasi_sekrng = "index";
    if (awal != 0) {
        mainView.router.loadPage('index.html');
        load_div_this_month("pertama");
    }

});
//myApp.onPageInit(*, function (page) {
myApp.onPageInit(null, function (page) {
    //alert(page.url); 
    lokasi_sekrng = "index";
    load_div_this_month("pertama");
    id_tab = 0;
    myApp.showTab('#tab2');
    var ptrContent = $$('.pull-to-refresh-content');
    ptrContent.on('refresh', function (e) {
        $$('#kolom_search').css('margin-top', '0px');
        $$("#search_index").val("").trigger("change");
        // Emulate 2s loading
        setTimeout(function () {

            if (id_tab == 1) {
                load_div_riwayat("pertama");
            } else if (id_tab == 2) {
                load_div_this_month("pertama");
            } else if (id_tab == 3) {
                load_div_month_besok("pertama");
            }
            // When loading done, we need to reset it
            //myApp.pullToRefreshDone();
        });
    });
});
var ck_waktu2 = "";
function load_div_this_month(a) {
    //alert("tess");
    var data_post = [];
    if (a == "pertama") {
        load_div_month_besok("pertama");
        load_div_riwayat("pertama");
        data_post = {id: ls_id, act_menu: "tab_dua", waktu: ""};
    } else {
        data_post = {id: ls_id, act_menu: "tab_dua", waktu: ck_waktu2};
    }

    $$.ajax({
        url: ipserver + 'menu/this_month.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            if (a == "pertama") {
                if (kiriman.jml_data > 0) {
                    isinya += '<li class="list-group-title div_this_month">' + kiriman.data[0][9] + ', ' + kiriman.data[0][3] + ' ' + kiriman.data[0][4] + '</li>';
                    for (var indx = 0; indx < kiriman.data.length; indx++) {

                        isinya += '<li class="kartu">'
                                + '<a class="item-link item-content b_edit" id_meet="' + kiriman.data[indx][0] + '" id_lead="' + kiriman.data[indx][6] + '" status="' + kiriman.data[indx][7] + '">'
                                + '<div class="item-inner">'
                                + '<div class="item-title-row">'
                                + '<div class="item-title text_panjang">';
                        if (kiriman.data[indx][7] == "batal") {
                            isinya += '<i class="fa fa-times-circle color-red" aria-hidden="true"></i>&nbsp;<strike>' + kiriman.data[indx][1] + '</strike>';
                        } else if (kiriman.data[indx][7] == "sudah") {
                            isinya += '<i class="fa fa-check-square-o color-green" aria-hidden="true"></i>&nbsp;' + kiriman.data[indx][1];
                        } else {
                            isinya += kiriman.data[indx][1];
                        }
                        isinya += '</div>'
                                + '<div class="item-after"> ' + kiriman.data[indx][5] + ' WIB </div>'
                                + '</div>'
                                + '<div class="item-subtitle color-green"><span class="color-gray">Place: ' + kiriman.data[indx][8] + ' </span><br/>Lead : ' + kiriman.data[indx][2] + ' </div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_this_month_atas").html(isinya);
                } else {
                    $$("#div_this_month_atas").html('<li class="list-group-title div_this_month">Data Not Found</li>');
                }
            }
            else if (a == "auto") {

                //alert($$(".div_this_month").text());
                if ($$(".div_this_month").text() == "Data Not Found") {
                    isinya = '<li class="list-group-title div_this_month">' + kiriman.data[0][3] + ' ' + kiriman.data[0][4] + '</li>';
                    $$("#div_this_month_atas").html(isinya);
                }

                if (kiriman.jml_data > 0) {
                    load_div_this_month("pertama");
                }

            }


            ck_waktu2 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');
        },
        complete: function () {
            //myApp.hideIndicator();

        }
    });
}


var ck_jml2_besok = 0;
var ck_detik_besok = "";
function load_div_month_besok(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        ck_jml2_besok = 0;
        ck_detik_besok = "";
        data_post = {id: ls_id, act_menu: "tab_tiga", jml_data: ck_jml2_besok, detik: ck_detik_besok};
    } else {
        data_post = {id: ls_id, act_menu: "tab_tiga", jml_data: ck_jml2_besok, detik: ck_detik_besok};
    }

    $$.ajax({
        url: ipserver + 'menu/this_besok.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {

                if (kiriman.jml_data != ck_jml2_besok || ck_detik_besok != kiriman.detik) {

                    $.each(kiriman.group, function (key, value) {
                        //alert(value);
                        //alert(kiriman.groupxx[value].count);
                        isinya += '<li class="list-group-title">' + value + '</li>';
                        for (var a1 = 0; a1 < kiriman.group_data[value].count; a1++) {
                            //alert(kiriman.group_data[value].count);

                            isinya += '<li class="kartu_besok">';
                            //if (kiriman.group_data[value].items[a1][6] == ls_id) {
                            if (kiriman.group_data[value].items[a1][7] == 'batal') {
                                //isinya += '<a class="item-link item-content tahan_hapus b_edit" id_meet="' + kiriman.group_data[value].items[a1][0] + '" id_lead="' + kiriman.group_data[value].items[a1][6] + '" status="besok">';
                                isinya += '<a class="item-link item-content b_edit" id_meet="' + kiriman.group_data[value].items[a1][0] + '" id_lead="' + kiriman.group_data[value].items[a1][6] + '" status="batal">';
                            } else {
                                isinya += '<a class="item-link item-content b_edit" id_meet="' + kiriman.group_data[value].items[a1][0] + '" id_lead="' + kiriman.group_data[value].items[a1][6] + '" status="besok">';
                            }

                            isinya += '<div class="item-media color-red">' + kiriman.group_data[value].items[a1][9]
                                    + ',<br/>' + kiriman.group_data[value].items[a1][3]
                                    + '<div style="display:none;">' + value + '</div>'
                                    + '</div>'
                                    + '<div class="item-inner" style="margin-left:0px;">'
                                    + '<div class="item-title-row">';
                            if (kiriman.group_data[value].items[a1][7] == 'batal') {
                                isinya += '<div class="item-title text_panjang"><strike>' + kiriman.group_data[value].items[a1][1] + '</strike></div>';
                            } else {
                                isinya += '<div class="item-title text_panjang">' + kiriman.group_data[value].items[a1][1] + '</div>';
                            }
                            isinya += '<div class="item-after">' + kiriman.group_data[value].items[a1][5] + ' WIB</div>'
                                    + '</div>'
                                    + '<div class="item-subtitle color-green">'
                                    + '<span class="color-gray">Place: ' + kiriman.group_data[value].items[a1][8] + ' </span><br/>Lead : ' + kiriman.group_data[value].items[a1][2] + '</div>'
                                    + '</div>'
                                    + ' </a>'
                                    + '</li>';
                        }

                    });
                    $$("#div_tab3besok_atas").html(isinya);
                }
            } else {

                $$("#div_tab3besok_atas").html('<li class="list-group-title">Data Not Found</li>');
            }
            //   } else if (a == "auto") {
            //  }


            ck_jml2_besok = kiriman.jml_data;
            ck_detik_besok = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');
            myApp.pullToRefreshDone();
        },
        complete: function () {
            //myApp.hideIndicator();
            myApp.pullToRefreshDone();
        }
    });
}

var ck_jml2_riwayat = 0;
function load_div_riwayat(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        ck_jml2_riwayat = 0;
        data_post = {id: ls_id, act_menu: "tab_satu", jml_data: ck_jml2_riwayat};
    } else {
        data_post = {id: ls_id, act_menu: "tab_satu", jml_data: ck_jml2_riwayat};
    }

    $$.ajax({
        url: ipserver + 'menu/this_history.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {

                if (kiriman.jml_data != ck_jml2_riwayat) {

                    $.each(kiriman.group, function (key, value) {
                        //alert(value);
                        //alert(kiriman.groupxx[value].count);
                        isinya += '<li class="list-group-title">' + value + '</li>';
                        for (var a1 = 0; a1 < kiriman.group_data[value].count; a1++) {
                            //alert(kiriman.group_data[value].count);
                            if (kiriman.group_data[value].items[a1][7] == "batal") {
                                isinya += '<li>'
                                        + '<a class="item-link item-content b_edit" id_meet="' + kiriman.group_data[value].items[a1][0] + '" id_lead="' + kiriman.group_data[value].items[a1][6] + '" status="' + kiriman.group_data[value].items[a1][7] + '"">'
                                        + '<div class="item-media color-red">' + kiriman.group_data[value].items[a1][9] + ',<br/>' + kiriman.group_data[value].items[a1][3]
                                        + '<div style="display:none;">' + value + '</div>'
                                        + '</div>'
                                        + '<div class="item-inner" style="margin-left:0px;">'
                                        + '<div class="item-title-row">'
                                        + '<div class="item-title text_panjang"><strike>' + kiriman.group_data[value].items[a1][1] + '</strike></div>'
                                        + '<div class="item-after">' + kiriman.group_data[value].items[a1][5] + ' WIB</div>'
                                        + '</div>'
                                        + '<div class="item-subtitle color-green"><span class="color-gray">Place: ' + kiriman.group_data[value].items[a1][8] + ' </span><br/>Lead : ' + kiriman.group_data[value].items[a1][2] + '</div>'
                                        + '</div>'
                                        + ' </a>'
                                        + '</li>';
                            } else {
                                isinya += '<li>'
                                        + '<a class="item-link item-content b_edit" id_meet="' + kiriman.group_data[value].items[a1][0] + '" id_lead="' + kiriman.group_data[value].items[a1][6] + '" status="' + kiriman.group_data[value].items[a1][7] + '"">'
                                        + '<div class="item-media color-red">' + kiriman.group_data[value].items[a1][9] + ',<br/>' + kiriman.group_data[value].items[a1][3]
                                        + '<div style="display:none;">' + value + '</div>'
                                        + '</div>'
                                        + '<div class="item-inner" style="margin-left:0px;">'
                                        + '<div class="item-title-row">'
                                        + '<div class="item-title text_panjang">' + kiriman.group_data[value].items[a1][1] + '</div>'
                                        + '<div class="item-after">' + kiriman.group_data[value].items[a1][5] + ' WIB</div>'
                                        + '</div>'
                                        + '<div class="item-subtitle color-green"><span class="color-gray">Place: ' + kiriman.group_data[value].items[a1][8] + ' </span><br/>Lead : ' + kiriman.group_data[value].items[a1][2] + '</div>'
                                        + '</div>'
                                        + ' </a>'
                                        + '</li>';
                            }
                        }

                    });
                    $$("#div_this_history").html(isinya);
                }
            } else {

                $$("#div_this_history").html('<li class="list-group-title">Data Not Found</li>');
            }
            //   } else if (a == "auto") {
            //  }


            ck_jml2_riwayat = kiriman.jml_data;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');
            myApp.pullToRefreshDone();
        },
        complete: function () {
            //myApp.hideIndicator();
            myApp.pullToRefreshDone();
        }
    });
}


//show
var id_tab = 0;
$$(document).on('show', '#tab1', function () {
    $$('#kolom_search').css('margin-top', '0px');
    $$("#search_index").val("").trigger("change");
    if (id_tab != 1) {
        $$("#b_add_not").find('i').toggleClass('icon icon-plus fa fa-sort-amount-desc');
        $$("#b_add_not").toggleClass('color-purple color-red');
    }
    id_tab = 1;
});
$$(document).on('show', '#tab2', function () {
    $$('#kolom_search').css('margin-top', '0px');
    $$("#search_index").val("").trigger("change");
    if (id_tab == 1) {
        $$("#b_add_not").find('i').toggleClass('fa fa-sort-amount-desc icon icon-plus');
        $$("#b_add_not").toggleClass('color-red color-purple');
    }

    id_tab = 2;
    terima_notif();
});
$$(document).on('show', '#tab3', function () {
    $$('#kolom_search').css('margin-top', '0px');
    $$("#search_index").val("").trigger("change");
    //$$("#b_add_not").find('i').toggleClass('fa fa-sort-amount-desc icon icon-plus');
    if (id_tab == 1) {
        $$("#b_add_not").find('i').toggleClass('fa fa-sort-amount-desc icon icon-plus');
        $$("#b_add_not").toggleClass('color-red color-purple');
    }
    id_tab = 3;
    terima_notif();
});
//paskan posisi ke 2
myApp.showTab('#tab2');
var ptrContent = $$('.pull-to-refresh-content');
// Add 'refresh' listener on it
ptrContent.on('refresh', function (e) {
    $$('#kolom_search').css('margin-top', '0px');
    $$("#search_index").val("").trigger("change");
    // Emulate 2s loading
    setTimeout(function () {

        if (id_tab == 1) {
            load_div_riwayat("pertama")
        } else if (id_tab == 2) {
            load_div_this_month("pertama");
        }
        else if (id_tab == 3) {
            load_div_month_besok("pertama");
        }
        myApp.pullToRefreshDone();
    });
});
var hari_max_flt = new Date();
hari_max_flt.setDate(hari_max_flt.getDate() - 1);
var bulan_min_flt = new Date();
bulan_min_flt.setMonth(bulan_min_flt.getMonth() - 3);
var calendarDateFormat = myApp.calendar({
    input: '#tgl_mulai',
    closeOnSelect: true,
    value: [bulan_min_flt],
    maxDate: [hari_max_flt],
    dateFormat: 'dd MM yyyy'
});
var calendarDateFormat = myApp.calendar({
    input: '#tgl_akhir',
    closeOnSelect: true,
    value: [hari_max_flt],
    maxDate: [hari_max_flt],
    dateFormat: 'dd MM yyyy'
});
$$(document).on("click", "#b_add_not", function () {
    if (id_tab == 1) {
        myApp.popup('.filter-history');
    } else {
        mainView.router.loadPage('add_notulen.html');
    }
});
$$(document).on("click", "#b_filer_history", function () {

    $$.ajax({
        url: ipserver + 'menu/this_history.php',
        method: "POST",
        data: {
            id: ls_id,
            act_menu: "tab_satu_filter",
            tgl_awal: $$("#tgl_mulai").val(),
            tgl_akhir: $$("#tgl_akhir").val()},
        dataType: "json",
        timeout: 10 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            if (kiriman.jml_data > 0) {

                //   if (kiriman.jml_data != ck_jml2_riwayat) {

                $.each(kiriman.group, function (key, value) {

                    isinya += '<li class="list-group-title">' + value + '</li>';
                    for (var a1 = 0; a1 < kiriman.group_data[value].count; a1++) {
                        //alert(kiriman.group_data[value].count);
                        if (kiriman.group_data[value].items[a1][7] == "batal") {
                            isinya += '<li>'
                                    + '<a class="item-link item-content b_edit" id_meet="' + kiriman.group_data[value].items[a1][0] + '" id_lead="' + kiriman.group_data[value].items[a1][6] + '" status="' + kiriman.group_data[value].items[a1][7] + '"">'
                                    + '<div class="item-media color-red">' + kiriman.group_data[value].items[a1][9] + ',<br/>' + kiriman.group_data[value].items[a1][3]
                                    + '<div style="display:none;">' + value + '</div>'
                                    + '</div>'
                                    + '<div class="item-inner" style="margin-left:0px;">'
                                    + '<div class="item-title-row">'
                                    + '<div class="item-title text_panjang"><strike>' + kiriman.group_data[value].items[a1][1] + '</strike></div>'
                                    + '<div class="item-after">' + kiriman.group_data[value].items[a1][5] + ' WIB</div>'
                                    + '</div>'
                                    + '<div class="item-subtitle color-green"><span class="color-gray">Place: ' + kiriman.group_data[value].items[a1][8] + ' </span><br/>Lead : ' + kiriman.group_data[value].items[a1][2] + '</div>'
                                    + '</div>'
                                    + ' </a>'
                                    + '</li>';
                        } else {
                            isinya += '<li>'
                                    + '<a class="item-link item-content b_edit" id_meet="' + kiriman.group_data[value].items[a1][0] + '" id_lead="' + kiriman.group_data[value].items[a1][6] + '" status="' + kiriman.group_data[value].items[a1][7] + '"">'
                                    + '<div class="item-media color-red">' + kiriman.group_data[value].items[a1][9] + ',<br/>' + kiriman.group_data[value].items[a1][3]
                                    + '<div style="display:none;">' + value + '</div>'
                                    + '</div>'
                                    + '<div class="item-inner" style="margin-left:0px;">'
                                    + '<div class="item-title-row">'
                                    + '<div class="item-title text_panjang">' + kiriman.group_data[value].items[a1][1] + '</div>'
                                    + '<div class="item-after">' + kiriman.group_data[value].items[a1][5] + ' WIB</div>'
                                    + '</div>'
                                    + '<div class="item-subtitle color-green"><span class="color-gray">Place: ' + kiriman.group_data[value].items[a1][8] + ' </span><br/>Lead : ' + kiriman.group_data[value].items[a1][2] + '</div>'
                                    + '</div>'
                                    + ' </a>'
                                    + '</li>';
                        }
                    }

                });
                $$("#div_this_history").html(isinya);
                //  }
            } else {

                $$("#div_this_history").html('<li class="list-group-title">Data Not Found</li>');
            }


            myApp.closeModal('.filter-history');
            ck_jml2_riwayat = kiriman.jml_data;
        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
});

$$(document).on('taphold', '.tahan_hapus', function () {
    var id_mt = $$(this).attr('id_meet');
    var topic = $$(this).parents(".kartu_besok").find(".item-title").text();
    myApp.confirm('Are you sure want to Delete (' + topic + ')?', 'Kokola', function () {

        $$.ajax({
            url: ipserver + 'meeting/act_meeting.php',
            method: "POST",
            data: {op: "delete_meeting", id_meet: id_mt},
            dataType: "json",
            timeout: 3 * 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {
                if (kiriman == "ok") {
                    myApp.alert("Delete Success ", "Kokola", function () {
                        load_div_month_besok("pertama");
                    });
                } else {
                    myApp.alert("Failed", "Kokola");
                }

            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
            }
        });
    });
});


$$(document).on("click", ".panelkiri", function () {
    myApp.closePanel();
});
$$(document).on("click", "#b_cari_index", function () {
//alert("tes");
    $$('#kolom_search').css('margin-top', '46px');
});
$$(document).on("click", "#b_hapus_search", function () {
    $$('#kolom_search').css('margin-top', '0px');
    $$("#search_index").val("").trigger("change");
});
function log_out() {
    $$.post(ipserver + "update_apikey.php", {ep: ls_id, api: "logout"}, function (data) {
        var arr = $.parseJSON(data);
        //var arr = (data);
        if (arr.status == "sukses") {
            localStorage.clear();
            app.initialize();
            myApp.loginScreen();
        } else {
            alert("API Key gagal dihapus");
        }
    });
}
;
$$(document).on("click", ".log_out", function () {
// alert("log out");
    log_out();
});
$$(document).on("click", "#matikan", function () {
    myApp.confirm('are you sure want to exit?', 'Kokola', function () {
        navigator.app.exitApp();
    });
});
//$$(".b_edit").on("click",function (){
$$(document).on("click", ".b_edit", function () {
//alert($$(this).attr("id_meet")); 
    var idm = $$(this).attr("id_meet");
    var idl = $$(this).attr("id_lead");
    var sts = $$(this).attr("status");
    mainView.router.loadPage('edit_notulen.html?idm=' + idm + '&idl=' + idl + '&status=' + sts);
});
//$$(".b_keluar").on("click",function (){
$$(document).on("click", ".b_keluar", function () {
    navigator.app.exitApp();
});
//$$(".b_login").on("click",function (){
$$(document).on("click", ".b_login", function () {
//alert("tess login");
    if ($("#f_login").valid()) {
        var formData = myApp.formToJSON('#f_login');
        //alert(JSON.stringify(formData));
        $$.ajax({
            url: ipserver + 'action_login.php',
            method: "POST",
            data: formData,
            dataType: "json",
            timeout: 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {
                if (kiriman.id != null) {
                    //alert(kiriman.id+" "+kiriman.nama+" "+kiriman.email);
                    localStorage.setItem("ls_id", kiriman.id);
                    localStorage.setItem("ls_nama", kiriman.nama);
                    localStorage.setItem("ls_email", kiriman.email);
                    localStorage.setItem("ls_pass", kiriman.pass);
                    ls_id = localStorage.getItem("ls_id");
                    ls_nama = localStorage.getItem("ls_nama");
                    ls_email = localStorage.getItem("ls_email");
                    ls_pass = localStorage.getItem("ls_pass");




                    myApp.closeModal(".login-screen");
                    $$("#nama_profil").text(ls_nama);

                    setTimeout(function () {
                        update_apikey();
                    }, 5000);
                    load_div_this_month("pertama");
                } else {
                    alert("Wrong email or password");
                    //$$("#f_login")[0].reset();
                    localStorage.clear();
                }
            },
            error: function (data) {
                alert("No Connection");
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
            }
        });
    }
});
cek_login();
function cek_login() {
    ls_id = localStorage.getItem("ls_id");
    ls_nama = localStorage.getItem("ls_nama");
    ls_email = localStorage.getItem("ls_email");
    ls_pass = localStorage.getItem("ls_pass");
    //alert(ls_id);
    if (ls_id == "" || ls_id == null) {

    } else {
        $$.ajax({
            url: ipserver + 'action_login.php',
            method: "POST",
            data: {email: ls_email, password: ls_pass},
            dataType: "json",
            timeout: 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {
                if (kiriman.id != null) {
                    //alert(kiriman.id+" "+kiriman.nama+" "+kiriman.email);
                    localStorage.setItem("ls_id", kiriman.id);
                    localStorage.setItem("ls_nama", kiriman.nama);
                    localStorage.setItem("ls_email", kiriman.email);
                    localStorage.setItem("ls_pass", kiriman.pass);
                    ls_id = localStorage.getItem("ls_id");
                    ls_nama = localStorage.getItem("ls_nama");
                    ls_email = localStorage.getItem("ls_email");
                    ls_pass = localStorage.getItem("ls_pass");



                    myApp.closeModal(".login-screen");
                    $$("#nama_profil").text(ls_nama);
                    setTimeout(function () {
                        update_apikey();
                    }, 5000);
                    load_div_this_month("pertama");
                } else {
                    //alert("Wrong email or password");
                    $$("#f_login")[0].reset();
                    localStorage.clear();
                }
            },
            error: function (data) {
                alert("No Connection");
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
            }
        });
    }
}
;
var popup_sts = 2;
document.addEventListener("backbutton", function (e) {
    e.preventDefault();
    // navigator.app.exitApp();
    //alert("back fisik1"); 
    if (popup_sts == 1) {
        myApp.closeModal(".popup");
        popup_sts = 0;
    } else if (popup_sts == 2) {
        //myApp.confirm('Apakah Anda ingin menutup aplikasi?', 'Kokola', function () {
        //navigator.app.exitApp();
        //});                     
    } else if (popup_sts == 0) {
        //popup_sts=2;
        //mainView.router.back();
    } else if (popup_sts == 3) {
        //alert("pop pic2");
        //disabel klik button fisik
    }
}, false);

window.addEventListener('native.keyboardshow', keyboardShowHandler);
function keyboardShowHandler(e) {
    //alert('Keyboard show');
    myApp.hideToolbar(".toolbar-bottom");
}

window.addEventListener('native.hidekeyboard', keyboardHideHandler);
function keyboardHideHandler(e) {
    // alert('hide');
    myApp.showToolbar(".toolbar-bottom");
}


/* ===== Modals Page end index  ===== */



/* ===== Modals Page add_notulen  ===== */
myApp.onPageInit('add_notulen', function (page) {
//    alert("tess add topic");
    awal = 1;
    popup_sts = 0;
    lokasi_sekrng = "add_notulen";

    var waktu_meeting;
    //asli   
    waktu_meeting = myApp.picker({
        input: '#waktu',
        // rotateEffect: true,
        formatValue: function (p, values, displayValues) {
            return values[0] + ' : ' + values[1] + ' until ' + values[2] + ' : ' + values[3];
        },
        value: ['00', '00', '00', '00'],
        cols: [
            // Hours
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 23; i++) {
                        arr.push(i < 10 ? '0' + i : i);
                    }
                    return arr;
                })()
            },
            // Divider
            {
                divider: true,
                content: ':'
            },
            // Minutes
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 59; i += 15) {
                        arr.push(i < 10 ? '0' + i : i);
                        if (i == 45) {
                            arr.push(59);
                        }
                    }

                    return arr;
                })(),
            },
            // Divider
            {
                divider: true,
                content: 'until'
            },
            // Hours
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 23; i++) {
                        arr.push(i < 10 ? '0' + i : i);
                    }
                    return arr;
                })(),
            },
            // Divider
            {
                divider: true,
                content: ':'
            },
            // Minutes
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 59; i += 15) {
                        arr.push(i < 10 ? '0' + i : i);
                        if (i == 45) {
                            arr.push(59);
                        }
                    }
                    return arr;
                })(),
            }
        ],
        onClose: function () {
            anggota_new($$("#tanggal").val(), $$("#waktu").val(), "destroy");
        }
    });
    //end asli


    function cek_waktu_meeting(tgl_meeting, id_tempat_meeting) {
        //modiff
        $$.ajax({
            url: ipserver + 'meeting/cek_jam.php',
            method: "POST",
            data: {op: "cek_jam", tgl: tgl_meeting, id_tempat: id_tempat_meeting},
            dataType: "json",
            timeout: 3 * 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {

                var misal = kiriman;
//                var misal = [
//                    ['07', '30', '09', '30'],
//                    ['10', '45', '12', '00'],
//                    ['14', '00', '16', '15'],
//                    ['16', '15', '17', '45'],
//                    ['18', '15', '20', '30']
//                ];

                waktu_meeting.destroy();
                waktu_meeting = myApp.picker({
                    input: '#waktu',
                    // rotateEffect: true,
                    formatValue: function (p, values, displayValues) {
                        return values[0] + ' : ' + values[1] + ' until ' + values[2] + ' : ' + values[3];
                    },
                    value: ['00', '00', '00', '00'],
                    cols: [
                        // Hours
                        {
                            values: (function () {
                                var arr = [];
                                for (var i = 0; i <= 23; i++) {
                                    for (var vr = 0; vr < misal.length; vr++) {
                                        if (i == misal[vr][0]) {
//                                if(misal[vr][1]!='00'){
//                                 arr.push(i < 10 ? '0' + i : i);
//                                }
                                            for (var ck = misal[vr][0]; ck < misal[vr][2]; ck++) {
                                                i++;

                                            }
                                        }
                                    }
                                    arr.push(i < 10 ? '0' + i : i);
                                }
                                return arr;
                            })(),
                            onChange: function (picker, jam1) {
                                var menit = [];
                                var masuk_ck = '0';

                                //ada di jam2
                                for (var vr = 0; vr < misal.length; vr++) {
                                    //ada di jam2
                                    if (jam1 == misal[vr][2]) {
                                        for (var i = Number(misal[vr][3]); i <= 59; i += 15) {
                                            menit.push(i < 10 ? '0' + i : i);
                                        }
                                        masuk_ck = '1';
                                    }

                                    //ada di jam1
//                        else if (jam1 == misal[vr][0]) {
//                            for (var i = 0; i < Number(misal[vr][1]); i += 15) {
//                                menit.push(i < 10 ? '0' + i : i);
//                            }
//                           masuk_ck='1'; 
//                        }

                                }
                                //tdk ada yg masuk menit
                                if (masuk_ck == '0') {
                                    for (var i = 0; i <= 59; i += 15) {
                                        menit.push(i < 10 ? '0' + i : i);
                                    }
                                }

                                if (picker.cols[2].replaceValues) {
                                    picker.cols[2].replaceValues(menit);
                                }

                                //jam2
                                var jam2 = [];
                                var menit2 = [];
                                var masuk_jam2 = '0';
                                for (var vr = 0; vr < misal.length; vr++) {
                                    //untuk jam2
                                    if (jam1 < misal[vr][0]) {
                                        for (var iw = Number(jam1); iw <= misal[vr][0]; iw++) {
                                            jam2.push(iw < 10 ? '0' + iw : iw);
                                        }
                                        if (picker.cols[4].replaceValues) {
                                            picker.cols[4].replaceValues(jam2);
                                        }

                                        masuk_jam2 = '1';
                                        break;
                                    }


                                }
                                if (masuk_jam2 == '0') {
                                    //untuk jam2
                                    for (var iw = Number(jam1); iw <= 23; iw++) {
                                        jam2.push(iw < 10 ? '0' + iw : iw);
                                    }
                                    if (picker.cols[4].replaceValues) {
                                        picker.cols[4].replaceValues(jam2);
                                    }


                                }

                            }
                        },
                        // Divider
                        {
                            divider: true,
                            content: ':'
                        },
                        // Minutes
                        {
                            values: (function () {
                                var arr = [];
                                for (var i = 0; i <= 59; i += 15) {
                                    arr.push(i < 10 ? '0' + i : i);
                                    if (i == 45) {
                                        arr.push(59);
                                    }
                                }

                                return arr;
                            })(),
                        },
                        // Divider
                        {
                            divider: true,
                            content: 'until'
                        },
                        // Hours
                        {
                            values: (function () {
                                var arr = [];
                                for (var i = 0; i <= 23; i++) {
                                    arr.push(i < 10 ? '0' + i : i);
                                }
                                return arr;
                            })(),
                            onChange: function (picker, jam2) {
                                var menit = [];
                                var masuk_ck = '0';

                                for (var vr = 0; vr < misal.length; vr++) {
                                    //untuk jam2
                                    if (jam2 == misal[vr][0]) {
                                        for (var iw = 0; iw <= misal[vr][1]; iw += 15) {
                                            menit.push(iw < 10 ? '0' + iw : iw);
                                        }
                                        if (picker.cols[6].replaceValues) {
                                            picker.cols[6].replaceValues(menit);
                                        }

                                        masuk_ck = '1';
                                    }
                                }

                                if (masuk_ck == '0') {
                                    for (var iw = 0; iw <= 59; iw += 15) {
                                        menit.push(iw < 10 ? '0' + iw : iw);
                                    }
                                    if (picker.cols[6].replaceValues) {
                                        picker.cols[6].replaceValues(menit);
                                    }
                                }


                            }
                        },
                        // Divider
                        {
                            divider: true,
                            content: ':'
                        },
                        // Minutes
                        {
                            values: (function () {
                                var arr = [];
                                for (var i = 0; i <= 59; i += 15) {
                                    arr.push(i < 10 ? '0' + i : i);

                                }
                                return arr;
                            })(),
                        }
                    ],
                    onClose: function () {
                        anggota_new($$("#tanggal").val(), $$("#waktu").val(), "destroy");
                    }
                });
                //end modif
            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
            }
        });





    }


    $("#f_tambah").validate({
        rules: {
            lead: {
                required: true
            },
            tanggal: {
                required: true
            },
            waktu: {
                required: true
            },
            tempat: {
                required: true
            },
            topic: {
                required: true
            },
            tools: {
                required: true
            },
            anggota: {
                required: true
            }

        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    var hari_mulai = new Date();
    hari_mulai.setDate(hari_mulai.getDate() - 1);
    $$("#lead").val(ls_nama).trigger("change");
    $$(".id_user").val(ls_id);
    var calendarDateFormat = myApp.calendar({
        input: '#tanggal',
        closeOnSelect: true,
        value: [new Date()],
        minDate: hari_mulai,
        dateFormat: 'dd MM yyyy'
    });

    $$("#tanggal").on("change", function () {
        //alert($$("#tanggal").val());
        anggota_new($$("#tanggal").val(), $$("#waktu").val(), "destroy");

        if ($$("#tempat").val() != "" && $$("#tempat").val() != null) {
            cek_waktu_meeting($$("#tanggal").val(), $$('#id_tempat').val());
        }

    });

    var today = new Date();


    // Fruits data demo array
    var tempat;
    var kapasitas_ruang = 0;
    $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_tempat"}, function (datanya) {
        var data_auto_ajax = $.parseJSON(datanya);
        tempat = myApp.autocomplete({
            openIn: 'popup', //open in page
            opener: $$('#tempat'), //link that opens autocomplete
            preloader: true, //enable preloader
            valueProperty: 'id', //object's "value" property name
            textProperty: 'name', //object's "text" property name
            backOnSelect: true, // expand input
            searchbarPlaceholderText: "Search...",
            pageTitle: "Choose Place",
            //limit: 20, //limit to 20 results
            source: function (autocomplete, query, render) {
                var results = [];

                // Find matched items
                for (var i = 0; i < data_auto_ajax.length; i++) {
                    if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                        results.push(data_auto_ajax[i]);
                }
                // Render items by passing array with result items
                render(results);
            },
            onChange: function (autocomplete, value) {
                //alert(value[0].id);
                $$('#tempat').val(value[0].name).trigger('change');
                $$('#id_tempat').val(value[0].id);
                kapasitas_ruang = value[0].kapasitas;

                cek_waktu_meeting($$("#tanggal").val(), value[0].id);
            },
            onOpen: function () {
                $$(".searchbar-input input").val("a").trigger("change");
                setTimeout(function () {
                    $$(".searchbar-input input").val("").trigger("change");
                }, 10);
                popup_sts = 1;
            },
            onClose: function () {
                popup_sts = 0;
            }

        });
    });

    var anggota_auto_completed;
    var jumlah_peserta = 0;


    anggota_new($$("#tanggal").val(), $$("#waktu").val(), "awal");


    function anggota_new(tgl, wk, cek) {
        myApp.showIndicator();
        $$.post(ipserver + "meeting/act_meeting.php",
                {op: "baca_anggota_yg_bisa_atau_tidak", id: ls_id, tgl_m: tgl, waktu: wk},
        function (datanya) {

            myApp.hideIndicator();

            var data_auto_ajax = $.parseJSON(datanya);

            var data_dipilih = [];
            var tmpng_nama = "";
            if (cek == "destroy") {
                anggota_auto_completed.destroy();

                var id_terpilih = $$('#id_anggota').val().split(',');
                //alert(id_terpilih.length);

                $.each(data_auto_ajax, function (indx, value) {
                    for (var i = 0; i < id_terpilih.length; i++) {
                        if (id_terpilih[i] == value.id) {
                            data_dipilih.push({id: value.id, name: value.name});
                            if (tmpng_nama == "") {
                                tmpng_nama += value.name;
                            } else {
                                tmpng_nama += ", " + value.name;
                            }

                        }
                    }
                });


                // $$('#anggota').val(tmpng_nama).trigger('change');
                if (tmpng_nama == "") {
                    $$('#anggota_nw').html('<span class="color-gray">Participants</span>');
                } else {
                    $$('#anggota_nw').html(tmpng_nama);
                }
            }


            anggota_auto_completed = myApp.autocomplete({
                openIn: 'popup', //open in page
                opener: $$('#anggota_nw'), //link that opens autocomplete
                multiple: true, //allow multiple values
                valueProperty: 'id', //object's "value" property name
                textProperty: 'name', //object's "text" property name
                searchbarPlaceholderText: "Search...",
                pageTitle: "Choose Participants",
                value: data_dipilih,
                // limit: 20, //limit to 20 results
                source: function (autocomplete, query, render) {
                    var results = [];

                    // Find matched items
                    for (var i = 0; i < data_auto_ajax.length; i++) {
                        if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                            results.push(data_auto_ajax[i]);
                    }
                    // Render items by passing array with result items
                    render(results);
                },
                onChange: function (autocomplete, value) {
                    //alert(JSON.stringify(value));
                    //alert(value[0].id);
                    var id_ctt = "";
                    var value2 = "";
                    jumlah_peserta = 0;
                    for (var i = 0; i < value.length; i++) {
                        if (i == 0) {
                            value2 += value[i].name;
                            id_ctt += value[i].id;
                        } else {
                            value2 += ", " + value[i].name;
                            id_ctt += "," + value[i].id;
                        }
                        jumlah_peserta++;
                    }

                    //   $$('#anggota').val(value2).trigger('change');
                    if (value2 == "") {
                        $$('#anggota_nw').html('<span class="color-gray">Participants</span>');
                    } else {
                        $$('#anggota_nw').html(value2);
                    }
                    $$('#id_anggota').val(id_ctt);
                },
                onOpen: function () {
                    if ($$('#anggota_nw').text() == "Participants") {
//                    if ($$('#anggota').val() == "") {
                        $$(".searchbar-input input").val("a").trigger("change");
                        setTimeout(function () {
                            $$(".searchbar-input input").val("").trigger("change");
                        }, 10);
                    }
                    popup_sts = 1;
                },
                onClose: function () {
                    popup_sts = 0;
                }
            });
        });
    }

    $$(".b_sel_all").on("click", function () {
        anggota_auto_completed.destroy();

        myApp.showIndicator();
        $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_yg_bisa_atau_tidak", id: ls_id, tgl_m: $$("#tanggal").val(), waktu: $$("#waktu").val()}, function (datanya) {
            var data_auto_ajax = $.parseJSON(datanya);
            myApp.hideIndicator();

            var id_ctt1 = "";
            var value21 = "";
            jumlah_peserta = 0;

            var ix = 0;
            $.each(data_auto_ajax, function (key, value) {

                if (ix == 0) {
                    value21 += value.name;
                    id_ctt1 += value.id;
                } else {
                    value21 += ", " + value.name;
                    id_ctt1 += "," + value.id;
                }
                jumlah_peserta++;

                ix++;
            });

            //   $$('#anggota').val(value21).trigger('change');
            $$('#anggota_nw').html(value21);
            $$('#id_anggota').val(id_ctt1);


            anggota_auto_completed = myApp.autocomplete({
                openIn: 'popup', //open in page
                opener: $$('#anggota_nw'), //link that opens autocomplete
                multiple: true, //allow multiple values
                valueProperty: 'id', //object's "value" property name
                textProperty: 'name', //object's "text" property name
                searchbarPlaceholderText: "Search...",
                pageTitle: "Choose Participants",
                value: data_auto_ajax,
                // limit: 20, //limit to 20 results
                source: function (autocomplete, query, render) {
                    var results = [];

                    // Find matched items
                    for (var i = 0; i < data_auto_ajax.length; i++) {
                        if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                            results.push(data_auto_ajax[i]);
                    }
                    // Render items by passing array with result items
                    render(results);
                },
                onChange: function (autocomplete, value) {
                    //alert(JSON.stringify(value));
                    //alert(value[0].id);
                    var id_ctt = "";
                    var value2 = "";
                    jumlah_peserta = 0;
                    for (var i = 0; i < value.length; i++) {
                        if (i == 0) {
                            value2 += value[i].name;
                            id_ctt += value[i].id;
                        } else {
                            value2 += ", " + value[i].name;
                            id_ctt += "," + value[i].id;
                        }
                        jumlah_peserta++;
                    }

                    //   $$('#anggota').val(value2).trigger('change');
                    if (value2 == "") {
                        $$('#anggota_nw').html('<span class="color-gray">Participants</span>');
                    } else {
                        $$('#anggota_nw').html(value2);
                    }
                    $$('#id_anggota').val(id_ctt);
                },
                onOpen: function () {
//                    if ($$('#anggota').val() == "") {
                    if ($$('#anggota_nw').text() == "Participants") {
                        $$(".searchbar-input input").val("a").trigger("change");
                        setTimeout(function () {
                            $$(".searchbar-input input").val("").trigger("change");
                        }, 10);
                    }
                    popup_sts = 1;
                },
                onClose: function () {
                    popup_sts = 0;
                }
            });

            myApp.swipeoutClose(".swipeout");
        });

    });

    $$(".b_des_all").on("click", function () {
        anggota_auto_completed.destroy();
        myApp.showIndicator();
        $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_yg_bisa_atau_tidak", id: ls_id, tgl_m: $$("#tanggal").val(), waktu: $$("#waktu").val()}, function (datanya) {
            var data_auto_ajax = $.parseJSON(datanya);
            myApp.hideIndicator();
            // $$('#anggota').val("").trigger('change');
            $$('#anggota_nw').html('<span class="color-gray">Participants</span>');

            $$('#id_anggota').val("");


            anggota_auto_completed = myApp.autocomplete({
                openIn: 'popup', //open in page
                opener: $$('#anggota_nw'), //link that opens autocomplete
                multiple: true, //allow multiple values
                valueProperty: 'id', //object's "value" property name
                textProperty: 'name', //object's "text" property name
                searchbarPlaceholderText: "Search...",
                pageTitle: "Choose Participants",
                // limit: 20, //limit to 20 results
                source: function (autocomplete, query, render) {
                    var results = [];

                    // Find matched items
                    for (var i = 0; i < data_auto_ajax.length; i++) {
                        if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                            results.push(data_auto_ajax[i]);
                    }
                    // Render items by passing array with result items
                    render(results);
                },
                onChange: function (autocomplete, value) {
                    //alert(JSON.stringify(value));
                    //alert(value[0].id);
                    var id_ctt = "";
                    var value2 = "";
                    jumlah_peserta = 0;
                    for (var i = 0; i < value.length; i++) {
                        if (i == 0) {
                            value2 += value[i].name;
                            id_ctt += value[i].id;
                        } else {
                            value2 += ", " + value[i].name;
                            id_ctt += "," + value[i].id;
                        }
                        jumlah_peserta++;
                    }

                    $$('#anggota').val(value2).trigger('change');
                    $$('#anggota_nw').html(value2);
                    $$('#id_anggota').val(id_ctt);
                },
                onOpen: function () {
//                    if ($$('#anggota').val() == "") {
                    if ($$('#anggota_nw').text() == "Participants") {
                        $$(".searchbar-input input").val("a").trigger("change");
                        setTimeout(function () {
                            $$(".searchbar-input input").val("").trigger("change");
                        }, 10);
                    }
                    popup_sts = 1;
                },
                onClose: function () {
                    popup_sts = 0;
                }
            });

            myApp.swipeoutClose(".swipeout");
        });

    });

    var jumlah_peserta_ex = 0;
    $$("#b_simpan").on("click", function () {

        jumlah_peserta_ex = 0;
        $$("input[name*='anggota2[]']").each(function () {
            if ($$(this).val() != "") {
                jumlah_peserta_ex++;
            }
        });

        if ($("#f_tambah").valid()) {
            myApp.confirm('Are you sure?', 'Kokola', function () {
                if ((Number(jumlah_peserta) + Number(jumlah_peserta_ex)) <= kapasitas_ruang) {
                    var form = $$("#f_tambah")[0];
                    var formData = new FormData(form);

                    $$.ajax({
                        url: ipserver + 'meeting/act_meeting.php',
                        method: "POST",
                        data: formData,
                        dataType: "json",
                        timeout: 3 * 60 * 1000,
                        beforeSend: function () {
                            myApp.showIndicator();
                        },
                        success: function (kiriman) {
                            if (kiriman == "ok") {
                                myApp.alert("Save Success ", "Kokola", function () {
                                    mainView.router.loadPage('index.html');
                                    load_div_this_month("pertama");
                                });
                            } else if (kiriman == "pesan") {
                                myApp.alert("Time has been used, please change the time", "Kokola",
                                        function () {

                                            cek_waktu_meeting($$("#tanggal").val(), $$('#id_tempat').val());
                                            // load_div_this_month("pertama");
                                        });
                            }
                            else {
                                myApp.alert("Failed", "Kokola");
                            }

                        },
                        error: function (data) {
                            myApp.alert('No Connection or Error', 'Kokola');
                            myApp.hideIndicator();
                        },
                        complete: function () {
                            myApp.hideIndicator();
                        }
                    });
                } else {
                    myApp.alert("The number of participants exceeds "
                            + "the capacity of the room."
                            + "<br/>Total capacity = " + kapasitas_ruang
                            + "<br/>Total participants = " + (Number(jumlah_peserta) + Number(jumlah_peserta_ex)), "Kokola");

                }

            });
        }
    });
    $$("#tambah_anggota").on("click", function () {
//alert("tes tambah"); 
        var isinya = "";
        isinya = '<li class="swipeout">'
                + '<div class="swipeout-content item-content">'
                + '<div class="item-media delete_anggota"><i class="fa fa-users fa-2x color-brown"></i></div>'
                + '<div class="item-inner">'
                + '<div class="item-title floating-label">Anggota Eksternal (E-mail)</div>'
                + '<div class="item-input">'
                + '<input type="email" name="anggota2[]" placeholder=""/>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<div class="swipeout-actions-right">'
                + '<a href="#" class="swipeout-delete" data-confirm="Are you sure want to delete this item?" data-confirm-title="Delete?" data-close-on-cancel="true">Delete</a>'
                + '</div>'
                + '</div>'
                + '</li>';
        $$("#f_tambah").append(isinya);
    });

    $$(".b_tambah_tempat").on("click", function () {
        $$("#tempat_baru").val("").trigger("change");
        myApp.popup(".popup-tambah_tempat");
    });

    $("#f_tambah_tempat").validate({
        rules: {
            tempat_baru: {
                required: true,
                remote: ipserver + "meeting/cek_nama_tempat.php"
            }
        },
        messages: {
            tempat_baru: {
                remote: "Place already exists"
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    $$("#b_simpan_tambah_tempat").on("click", function () {
        if ($("#f_tambah_tempat").valid()) {
            //alert("b_simpan_tambah_tempat");
            $$.ajax({
                url: ipserver + 'meeting/act_meeting.php',
                method: "POST",
                data: {op: "tambah_tampat_meeting", nama_tempat: $$("#tempat_baru").val()},
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "ok") {
                        myApp.alert("Save Success ", "Kokola", function () {
                            $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_tempat"}, function (datanya) {
                                var data_auto_ajax = $.parseJSON(datanya);

                                tempat.destroy();
                                tempat = myApp.autocomplete({
                                    openIn: 'popup', //open in page
                                    opener: $$('#tempat'), //link that opens autocomplete
                                    preloader: true, //enable preloader
                                    valueProperty: 'id', //object's "value" property name
                                    textProperty: 'name', //object's "text" property name
                                    backOnSelect: true, // expand input
                                    searchbarPlaceholderText: "Search...",
                                    pageTitle: "Choose Place",
                                    //limit: 20, //limit to 20 results
                                    source: function (autocomplete, query, render) {
                                        var results = [];

                                        // Find matched items
                                        for (var i = 0; i < data_auto_ajax.length; i++) {
                                            if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                                                results.push(data_auto_ajax[i]);
                                        }
                                        // Render items by passing array with result items
                                        render(results);
                                    },
                                    onChange: function (autocomplete, value) {
                                        //alert(value[0].id);
                                        $$('#tempat').val(value[0].name).trigger('change');
                                        $$('#id_tempat').val(value[0].id);
                                        kapasitas_ruang = value[0].kapasitas;

                                    },
                                    onOpen: function () {
                                        $$(".searchbar-input input").val("a").trigger("change");
                                        setTimeout(function () {
                                            $$(".searchbar-input input").val("").trigger("change");
                                        }, 10);
                                        popup_sts = 1;
                                    },
                                    onClose: function () {
                                        popup_sts = 0;
                                    }

                                });

                                myApp.hideIndicator();
                                myApp.closeModal(".popup-tambah_tempat");
                            });


                        });
                    } else {
                        myApp.alert("Failed", "Kokola");
                    }
                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    //myApp.hideIndicator();
                }
            });
        }
    });

});
/* ===== Modals Page end add_notulen  ===== */



/* ===== Modals Page edit_notulen  ===== */
myApp.onPageInit('edit_notulen', function (page) {
    //alert(JSON.stringify(page.query)); 
    //alert("idmeet="+page.query.idm+" idlead="+page.query.idl);

    tinymce.remove('.tiny');
    tinymce.init({
        selector: '.tiny',
        menubar: false,
        statusbar: false
                //toolbar: 'undo redo | styleselect | bold italic | link image'
    });

    awal = 1;
    popup_sts = 0;
    lokasi_sekrng = "edit_notulen";
    localStorage.setItem("hak_swip", page.query.idl);
    localStorage.setItem("edit_notulen_idmeet", page.query.idm);
    localStorage.setItem("status_swip_meeting", page.query.status);
    $$(".id_meet_hidden").val(page.query.idm);
    if (page.query.idl != ls_id || page.query.status == "sudah" || page.query.status == "batal") {
        $$("#b_tambah").hide();
        $$("#b_finish").hide();
        $$("#b_cc_notulen").hide();

        $$("#div_edit_hapus").hide();
    }

    if (page.query.status == "besok") {
        $$("#b_tambah").hide();
        $$("#b_finish").hide();
        $$("#b_cc_notulen").hide();
    }

    cek_peserta_terkirim();
    cek_cc_peserta();

    $$.ajax({
        url: ipserver + 'meeting/act_meeting.php',
        method: "POST",
        data: {op: "baca_data_awal_meeting", id_meet: page.query.idm},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {

            $$(".txt_lead").text(kiriman[0]);
            $$(".txt_tgl").text(kiriman[1]);
            $$(".txt_waktu").text(kiriman[2]);
            $$(".txt_tempat").text(kiriman[3]);
            $$(".txt_topic").text(kiriman[4]);
            $$(".txt_tools").text(kiriman[5]);
        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });


    //absen bro
    var id_absen = "";
    var nama_absen = "";
    var anggota_absen;

    $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_absen", id_meet: page.query.idm}, function (datanya) {
        var data_auto_ajax = $.parseJSON(datanya);

        var yg_tdk_hadir = [];
        var nama_tdk_hadir = "";
        var id_tdk_hadir = "";

        var angka = 0;
        $.each(data_auto_ajax, function (indx, value) {

            if (value.hadir == '0') {
                yg_tdk_hadir.push({id: value.id, name: value.name});
                if (angka == 0) {
                    nama_tdk_hadir += value.name;
                    id_tdk_hadir += value.id;
                } else {
                    nama_tdk_hadir += ", " + value.name;
                    id_tdk_hadir += "," + value.id;
                }

                angka++;
            }

        });

        $$("#id_tdk_hadir").val(id_tdk_hadir);
        if (angka != 0) {
            $$("#nama_anggota_tidak_hadir").text(nama_tdk_hadir);
        } else {
            $$("#nama_anggota_tidak_hadir").text("-");
        }
        anggota_absen = myApp.autocomplete({
            openIn: 'popup', //open in page
            opener: $$('#b_absen'), //link that opens autocomplete
            multiple: true, //allow multiple values
            valueProperty: 'id', //object's "value" property name
            textProperty: 'name', //object's "text" property name
            searchbarPlaceholderText: "Search...",
            pageTitle: "List Participants",
            value: yg_tdk_hadir,
            source: function (autocomplete, query, render) {
                var results = [];
                // Find matched items
                for (var i = 0; i < data_auto_ajax.length; i++) {
                    if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                        results.push(data_auto_ajax[i]);
                }
                // Render items by passing array with result items
                render(results);
            },
            onChange: function (autocomplete, value) {
                //    alert("dipilih");
                id_absen = "";
                nama_absen = "";

                for (var i = 0; i < value.length; i++) {
                    if (i == 0) {
                        nama_absen += value[i].name;
                        id_absen += value[i].id;

                    } else {
                        nama_absen += ", " + value[i].name;
                        id_absen += "," + value[i].id;
                    }
                }
                console.log(nama_absen);
                console.log(id_absen);

                $$("#id_tdk_hadir").val(id_absen);
                if (nama_absen == "") {
                    $$("#nama_anggota_tidak_hadir").text("-");
                } else {
                    $$("#nama_anggota_tidak_hadir").text(nama_absen);
                }
                //$$(this).prop('checked', true);
            },
            onOpen: function () {
                $$(".searchbar-input input").val("a").trigger("change");
                setTimeout(function () {
                    $$(".searchbar-input input").val("").trigger("change");
                }, 10);

                popup_sts = 1;
            },
            onClose: function () {
                popup_sts = 0;
                absensi_peserta($$("#id_tdk_hadir").val(), page.query.idm);
            }
        });
    });
    //end absen


    function absensi_peserta(id_peserta, id_meeting) {
        // alert("asd");

        formData = {op: "absensi_peserta", id_m: id_meeting, id_p: id_peserta};

        $$.ajax({
            url: ipserver + 'meeting/act_meeting.php',
            method: "POST",
            data: formData,
            dataType: "json",
            timeout: 3 * 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {
                if (kiriman == "ok") {
//                    myApp.alert("Save Success", "Kokola", function () {
//                        
//                    });
                } else {
                    myApp.alert("Failed", "Kokola");
                }

            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
            }
        });
    }


    $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_terdaftar_user", id_meet: page.query.idm}, function (datanya) {

        var data_auto_ajax = $.parseJSON(datanya);

        data_auto_ajax.push({id: ls_id, name: ls_nama});

        var pic2 = myApp.autocomplete({
            openIn: 'page', //open in page
            opener: $$('#pic2'), //link that opens autocomplete
            preloader: true, //enable preloader
            valueProperty: 'id', //object's "value" property name
            textProperty: 'name', //object's "text" property name
            backOnSelect: true, // expand input
            searchbarPlaceholderText: "Search...",
            pageTitle: "Choose PIC",
            source: function (autocomplete, query, render) {
                var results = [];
                // Find matched items
                for (var i = 0; i < data_auto_ajax.length; i++) {
                    if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                        results.push(data_auto_ajax[i]);
                }
                // Render items by passing array with result items
                render(results);
            },
            onChange: function (autocomplete, value) {
                //alert(value[0].id);
                $$('#pic2').val(value[0].name).trigger('change');
                $$('#id_pic2').val(value[0].id);
            },
            onOpen: function () {
                $$(".searchbar-input input").val("a").trigger("change");
                setTimeout(function () {
                    $$(".searchbar-input input").val("").trigger("change");
                }, 10);
                popup_sts = 3;
            },
            onClose: function () {
                popup_sts = 1;
            }

        });
    });
    var hari_mulai = new Date();
    hari_mulai.setDate(hari_mulai.getDate() - 1);
    var calendarDateFormat2 = myApp.calendar({
        closeOnSelect: true,
        input: '#duedate2',
        value: [new Date()],
        minDate: hari_mulai,
        dateFormat: 'dd MM yyyy'
    });
//    calendarDateFormat.setValue(["2016-10-14"]);

    $$("#b_tambah").on("click", function () {
        popup_sts = 1;
        calendarDateFormat2.setValue([new Date()]);

        //ini tambah notulen
        tinymce.get('subtopic2').setContent("");

        $$("#subtopic2").val("").trigger("change");
        $$("#keterangan2").val("").trigger("change");
        myApp.popup(".popup-tambah");
    });

    $$('.popup-tambah').on('close', function () {
        popup_sts = 0;
    });
    $("#f_tambah_sub_topic").validate({
        rules: {
            subtopic2: {
                required: true
            },
            pic2: {
                required: true
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    $$("#b_simpan_tambah").on("click", function () {
        //alert("tambah");

        if ($("#f_tambah_sub_topic").valid()) {
            myApp.confirm('Are you sure?', 'Kokola', function () {

                //myApp.alert('You clicked Ok button');
                tinymce.triggerSave();
                var form = $$("#f_tambah_sub_topic")[0];
                var formData = new FormData(form);
                $$.ajax({
                    url: ipserver + 'meeting/act_submeeting.php',
                    method: "POST",
                    data: formData,
                    dataType: "json",
                    timeout: 3 * 60 * 1000,
                    beforeSend: function () {
                        myApp.showIndicator();
                    },
                    success: function (kiriman) {
                        if (kiriman == "ok") {
                            myApp.alert("Save Success ", "Kokola", function () {
                                load_sub_topic("reload");
                                myApp.closeModal(".popup-tambah");
                            });
                        } else {
                            myApp.alert("Failed", "Kokola");
                        }

                    },
                    error: function (data) {
                        myApp.alert('No Connection or Error', 'Kokola');
                        myApp.hideIndicator();
                    },
                    complete: function () {
                        myApp.hideIndicator();
                    }
                });
            });
        }

    });


    $$("#b_finish").on("click", function () {
        myApp.confirm("Are you sure closing meeting?<br/><br/><span class='color-red'>Note : Do not forget to pray before closing the meeting</span>", "Kokola", function () {

            $$.ajax({
                url: ipserver + 'meeting/act_submeeting.php',
                method: "POST",
                data: {op: "finish_meeting_v1.15", id_meet: localStorage.getItem("edit_notulen_idmeet"), id_anggota_cc: $$("#id_cc_notulen").val()},
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "ok") {
                        myApp.alert("Save Success ", "Kokola", function () {
                            mainView.router.loadPage('index.html');
                            load_div_this_month("pertama");
                        });
                    } else {
                        myApp.alert("Failed", "Kokola");
                    }

                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        });
    });

    $$("#b_edittopic").on("click", function () {
        mainView.router.loadPage('edit_anggota.html?idm=' + page.query.idm
                + '&idl=' + page.query.idl
                + '&status=' + page.query.status);
    });

    $$("#b_canceltopic").on("click", function () {
        myApp.confirm('Are you sure want to Cancel Meeting?', 'Kokola', function () {
            $$.ajax({
                url: ipserver + 'meeting/act_meeting.php',
                method: "POST",
                data: {op: "delete_meeting", id_meet: page.query.idm},
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "ok") {
                        myApp.alert("Success Cancel Meeting", "Kokola", function () {
                            mainView.router.loadPage("index.html");
                        });
                    } else {
                        myApp.alert("Failed", "Kokola");
                    }

                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        });
    });


    $$("#b_hapus_search_edit_notulen").on("click", function () {
        //alert("search tes");
        $$('#serach_edit_notulen').css('top', '0px');
        $$("#search_edit_notulen").val("").trigger("change");
    });
    $$("#tampil_search_edit_notulen").on("click", function () {
        $$('#serach_edit_notulen').css('top', '56px');
    });
    load_sub_topic("reload");

    //cc bro

    $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_belum_terdaftar", id_meet: page.query.idm, idl: page.query.idl}, function (datanya) {
        var data_auto_ajax = $.parseJSON(datanya);

        var auto_cc_notulen = myApp.autocomplete({
            openIn: 'page', //open in page
            opener: $$('#b_cc_notulen'), //link that opens autocomplete
            multiple: true, //allow multiple values
            valueProperty: 'id', //object's "value" property name
            textProperty: 'name', //object's "text" property name
            searchbarPlaceholderText: "Search...",
            pageTitle: "Choose Participants",
            // limit: 20, //limit to 20 results
            source: function (autocomplete, query, render) {
                var results = [];
                // Find matched items
                for (var i = 0; i < data_auto_ajax.length; i++) {
                    if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                        results.push(data_auto_ajax[i]);
                }
                // Render items by passing array with result items
                render(results);
            },
            onChange: function (autocomplete, value) {
                //alert(JSON.stringify(value));
                //alert(value[0].id);
                var id_ctt = "";
                var value2 = "";

                for (var i = 0; i < value.length; i++) {
                    if (i == 0) {
                        value2 += value[i].name;
                        id_ctt += value[i].id;
                    } else {
                        value2 += ", " + value[i].name;
                        id_ctt += "," + value[i].id;
                    }

                }

                if (value.length != 0) {
                    $$('#b_cc_notulen').text("(" + value.length + ") CC Notulen");
                } else {
                    $$('#b_cc_notulen').text("CC Notulen");
                }
                $$('#id_cc_notulen').val(id_ctt);
            },
            onOpen: function () {
                if ($$('#id_cc_notulen').val() == "") {
                    $$(".searchbar-input input").val("a").trigger("change");
                    setTimeout(function () {
                        $$(".searchbar-input input").val("").trigger("change");
                    }, 10);
                }
                popup_sts = 1;
            },
            onClose: function () {
                popup_sts = 0;
            }
        });
    });

    //end cc

});
var edit_waktu = "";
var edit_jumlah = "";
function load_sub_topic(a) {

    if (a == "reload") {
        edit_waktu = "";
        edit_jumlah = "";
    }

    $$.ajax({
        url: ipserver + 'meeting/baca_submeeting.php',
        method: "POST",
        data: {
            act_subtopic: "baca",
            id_meet: localStorage.getItem("edit_notulen_idmeet"),
            waktu: edit_waktu,
            jumlah: edit_jumlah
        },
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {



            if (kiriman.cek_baru != "0") {
                var isi_rapat = "";
                for (var indx = 0; indx < kiriman.data.length; indx++) {
                    if (localStorage.getItem("hak_swip") == ls_id
                            && localStorage.getItem("status_swip_meeting") != "sudah") {

                        isi_rapat += '<li class="accordion-item swipeout">'
                                + '<a href="#" class="item-content item-link swipeout-content">'
                                + '<div class="item-media no_rapat"><b class="color-indigo">' + (indx + 1) + '</b></div>'
                                + '<div class="item-inner awal">'
                                + '<div class="item-title color-indigo text_panjang">  '
                                + kiriman.data[indx][1]
                                + '</div>'
                                + '</div>'
                                + '</a>'
                                + '<div class="accordion-item-content swipeout-content">'
                                + '<div class="content-block">'
                                + '<div class="content-block">'
                                + '<div class="row">'
                                + '<div class="col-100 color-red">PIC'
                                + '<div class="row color-black border-lightblue pic garis">'
                                + kiriman.data[indx][5]
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '<div class="row">'
                                + ' <div class="col-100 color-red">Due Date'
                                + '<div class="row color-black border-lightblue duedate garis">'
                                + kiriman.data[indx][3]
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '<div class="row">'
                                + ' <div class="col-100 color-red">Category'
                                + '<div class="row color-black border-lightblue kategori garis">'
                                + (kiriman.data[indx][7])
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '<div class="swipeout-actions-right">'
                                + '<a href="#" class="action1 bg-orange b_edit2" unix_id="' + kiriman.data[indx][0] + '">Edit</a>'
                                + '<a href="#" class="b_hapus_subtopic bg-red" unix_id="' + kiriman.data[indx][0] + '">Delete</a>'
                                + '</div>'
                                + '</li>';
                    } else {
                        if (kiriman.data[indx][6] == '1') {
                            isi_rapat += '<li class="accordion-item bgimgdone">';
                        } else if (kiriman.data[indx][6] == '2') {
                            isi_rapat += '<li class="accordion-item bgimgrevisi">';
                        } else if (kiriman.data[indx][6] == '3') {
                            isi_rapat += '<li class="accordion-item bgimgclosed">';
                        } else {
                            isi_rapat += '<li class="accordion-item">';
                        }


                        isi_rapat += '<a href="#" class="item-content item-link">'
                                + '<div class="item-media no_rapat"><b class="color-indigo">' + (indx + 1) + '</b></div>'
                                + '<div class="item-inner awal">'
                                + '<div class="item-title color-indigo text_panjang">  '
                                + kiriman.data[indx][1]
                                + '</div>'
                                + '</div>'
                                + '</a>'
                                + '<div class="accordion-item-content">'
                                + '<div class="content-block">'
                                + '<div class="content-block">'
                                + '<div class="row">'
                                + '<div class="col-100 color-red">PIC'
                                + '<div class="row color-black border-lightblue pic garis">'
                                + kiriman.data[indx][5]
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '<div class="row">'
                                + ' <div class="col-100 color-red">Due Date'
                                + '<div class="row color-black border-lightblue duedate garis">'
                                + kiriman.data[indx][3]
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '<div class="row">'
                                + ' <div class="col-100 color-red">Category'
                                + '<div class="row color-black border-lightblue kategori garis">'
                                + (kiriman.data[indx][7])
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '</div>'
                                + '</li>';
                    }
                }

                $$("#div_rapat").html(isi_rapat);
            }


            edit_waktu = kiriman.detik;
            edit_jumlah = kiriman.jml_data;
        },
        error: function (data) {
            // myApp.alert('No Connection or Error', 'Kokola');
        },
        complete: function () {
            // myApp.hideIndicator();
        }
    });
}


function cek_peserta_terkirim() {
    $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_terdaftar", id_meet: localStorage.getItem("edit_notulen_idmeet")}, function (datanya) {
        var data_auto_ajax = $.parseJSON(datanya);
        var data_auto_value = $.parseJSON(datanya);
        var isi_anggota_m = "";
        $.each(data_auto_ajax, function (key, value) {
            if (value.notif == "1") {
                isi_anggota_m += "<div class='row color-green no-gutter'><div class='col-auto'><b>" + value.name + "</b>,</div></div>";
            } else {
                isi_anggota_m += "<div class='row no-gutter'><div class='col-auto'>" + value.name + ",</div></div>";
            }
        });

        $$('#id_anggota_m').html(isi_anggota_m);
    });
}

function cek_cc_peserta() {
    $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_cc_peserta", id_meet: localStorage.getItem("edit_notulen_idmeet")}, function (datanya) {
        var data_auto_ajax = $.parseJSON(datanya);

        var isi_anggota_m = "";
        $.each(data_auto_ajax, function (key, value) {
            if (value.notif == "1") {
                isi_anggota_m += "<div class='row color-green no-gutter'><div class='col-auto'><b>" + value.name + "</b>,</div></div>";
            } else {
                isi_anggota_m += "<div class='row no-gutter'><div class='col-auto'>" + value.name + ",</div></div>";
            }
        });


        //  alert(isi_anggota_m);
        $$('#cc_notulen').html(isi_anggota_m);
    });
}


$$(document).on("click", ".b_edit2", function () {
    var id_sub = $$(this).attr("unix_id");
    // mainView.router.loadPage('edit_subtopic.html?id_sub='+id_sub);
    mainView.router.loadPage('edit_subtopic.html?idm=' + localStorage.getItem("edit_notulen_idmeet")
            + '&idl=' + localStorage.getItem("hak_swip")
            + '&status=' + localStorage.getItem("status_swip_meeting")
            + '&id_sub=' + id_sub);
});
/* ===== Modals Page end edit_notulen  ===== */


/* ===== Modals Page pic  ===== */
myApp.onPageInit('pic', function (page) {
    awal = 1;
    lokasi_sekrng = "pic";
    popup_sts = 0;

    $$("#filter_finish_pic").hide();
    $$('#tab4').on('show', function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_pic").val("").trigger("change");
        $$("#filter_finish_pic").hide();
    });
    $$('#tab5').on('show', function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_pic").val("").trigger("change");
        $$("#filter_finish_pic").hide();
    });
    $$('#tab6').on('show', function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_pic").val("").trigger("change");
        $$("#filter_finish_pic").hide();
    });
    $$('#tab7').on('show', function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_pic").val("").trigger("change");
        $$("#filter_finish_pic").show();
    });
    $$("#b_hapus_search_t_pic").on("click", function () {
        //alert("search tes");
        //$$('#kolom_search_t_pic').css('top', '45px');
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_pic").val("").trigger("change");
    });
    $$("#tampil_search_t_pic").on("click", function () {
        myApp.hideNavbar(".navbar");
        myApp.hideToolbar(".tabbar");
    });
    load_pic_tab1("pertama");
    var ptrContent = $$('.pull-to-refresh-content');
// Add 'refresh' listener on it
    ptrContent.on('refresh', function (e) {
        // Emulate 2s loading
        setTimeout(function () {
            load_pic_tab4("pertama");
            //myApp.pullToRefreshDone();
        });
    });
    var hari_max_flt = new Date();
    hari_max_flt.setDate(hari_max_flt.getDate() - 1);
    var bulan_min_flt = new Date();
    bulan_min_flt.setMonth(bulan_min_flt.getMonth() - 3);
    var calendarDateFormat3 = myApp.calendar({
        input: '#tgl_mulai_p',
        closeOnSelect: true,
        value: [bulan_min_flt],
        maxDate: [hari_max_flt],
        dateFormat: 'dd MM yyyy'
    });
    var calendarDateFormat4 = myApp.calendar({
        input: '#tgl_akhir_p',
        closeOnSelect: true,
        value: [new Date()],
        dateFormat: 'dd MM yyyy'
    });
    $$("#b_filter_finish_pic").on("click", function () {
        myApp.popup(".filter-done-pic");
    });
    $$("#b_filter_done_pic").on("click", function () {
        // alert("tes serch");
        $$.ajax({
            url: ipserver + 'pic/div_done.php',
            method: "POST",
            data: {op: "done_filter", id_pic: ls_id, tgl_awal: $$("#tgl_mulai_p").val(), tgl_akhir: $$("#tgl_akhir_p").val()},
            dataType: "json",
            timeout: 5 * 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {
                var isinya = '';
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="b_baca_pic item-link item-content" d-subtopic="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + '<div class="item-title text_panjang">'
                                + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">Lead : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                        $$("#div_pic_tab4").html(isinya);
                    }

                } else {

                    $$("#div_pic_tab4").html('<li class="list-group-title">Data Not Found</li>');
                }

            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
                myApp.closeModal(".filter-done-pic");
            }
        });
    });
});
$$(document).on("click", ".b_upload_pic", function () {
    var id_sub = $$(this).attr("d-subtopic");
    mainView.router.loadPage('upload_pic.html?id_subtopic=' + id_sub);
});
$$(document).on("click", ".b_upload_pic_revisi", function () {
    var id_sub = $$(this).attr("d-subtopic");
    mainView.router.loadPage('upload_pic_revisi.html?id_subtopic=' + id_sub);
});
$$(document).on("click", ".b_baca_pic", function () {
    var id_sub = $$(this).attr("d-subtopic");
    mainView.router.loadPage('baca_pic.html?id_subtopic=' + id_sub);
});
var ck_jml_pic_tab1 = 0;
var ck_waktu_pic_tab1 = "";
function load_pic_tab1(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        load_pic_tab2("pertama");
        load_pic_tab3("pertama");
        load_pic_tab4("pertama");
        ck_jml_pic_tab1 = 0;
        ck_waktu_pic_tab1 = "";
        data_post = {id_pic: ls_id, op: "waiting", jumlah: ck_jml_pic_tab1, waktu: ck_waktu_pic_tab1};
    } else {
        data_post = {id_pic: ls_id, op: "waiting", jumlah: ck_jml_pic_tab1, waktu: ck_waktu_pic_tab1};
    }

    $$.ajax({
        url: ipserver + 'pic/div_waiting.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="b_upload_pic item-link item-content" d-subtopic="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + '<div class="item-title text_panjang">'
                                + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">Lead : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_pic_tab1").html(isinya);
                }

            } else {

                $$("#div_pic_tab1").html('<li class="list-group-title">Data Not Found</li>');
            }


            ck_jml_pic_tab1 = kiriman.jml_data;
            ck_waktu_pic_tab1 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');

        },
        complete: function () {
            //myApp.hideIndicator();

        }
    });
}

var ck_jml_pic_tab2 = 0;
var ck_waktu_pic_tab2 = "";
function load_pic_tab2(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        ck_jml_pic_tab2 = 0;
        ck_waktu_pic_tab2 = "";
        data_post = {id_pic: ls_id, op: "revisi", jumlah: ck_jml_pic_tab2, waktu: ck_waktu_pic_tab2};
    } else {
        data_post = {id_pic: ls_id, op: "revisi", jumlah: ck_jml_pic_tab2, waktu: ck_waktu_pic_tab2};
    }

    $$.ajax({
        url: ipserver + 'pic/div_revisi.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="b_upload_pic_revisi item-link item-content" d-subtopic="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + '<div class="item-title text_panjang">'
                                + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">Lead : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_pic_tab2").html(isinya);
                }

            } else {

                $$("#div_pic_tab2").html('<li class="list-group-title">Data Not Found</li>');
            }


            ck_jml_pic_tab2 = kiriman.jml_data;
            ck_waktu_pic_tab2 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');

        },
        complete: function () {
            //myApp.hideIndicator();

        }
    });
}

var ck_jml_pic_tab3 = "";
var ck_waktu_pic_tab3 = "";
function load_pic_tab3(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        ck_jml_pic_tab3 = "";
        ck_waktu_pic_tab3 = "";
        data_post = {id_pic: ls_id, op: "ready", jumlah: ck_jml_pic_tab3, waktu: ck_waktu_pic_tab3};
    } else {
        data_post = {id_pic: ls_id, op: "ready", jumlah: ck_jml_pic_tab3, waktu: ck_waktu_pic_tab3};
    }

    $$.ajax({
        url: ipserver + 'pic/div_ready.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="b_baca_pic item-link item-content" d-subtopic="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + '<div class="item-title text_panjang">'
                                + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">Lead : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_pic_tab3").html(isinya);
                }

            } else {

                $$("#div_pic_tab3").html('<li class="list-group-title">Data Not Found</li>');
            }


            ck_jml_pic_tab3 = kiriman.jml_data;
            ck_waktu_pic_tab3 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');

        },
        complete: function () {
            //myApp.hideIndicator();
        }
    });
}

var ck_jml_pic_tab4 = "";
var ck_waktu_pic_tab4 = "";
function load_pic_tab4(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        ck_jml_pic_tab4 = "";
        ck_waktu_pic_tab4 = "";
        data_post = {id_pic: ls_id, op: "done", jumlah: ck_jml_pic_tab4, waktu: ck_waktu_pic_tab4};
    } else {
        data_post = {id_pic: ls_id, op: "done", jumlah: ck_jml_pic_tab4, waktu: ck_waktu_pic_tab4};
    }

    $$.ajax({
        url: ipserver + 'pic/div_done.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="b_baca_pic item-link item-content" d-subtopic="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + '<div class="item-title text_panjang">'
                                + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">Lead : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_pic_tab4").html(isinya);
                }

            } else {

                $$("#div_pic_tab4").html('<li class="list-group-title">Data Not Found</li>');
            }


            ck_jml_pic_tab4 = kiriman.jml_data;
            ck_waktu_pic_tab4 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');
            myApp.pullToRefreshDone();
        },
        complete: function () {
            //myApp.hideIndicator();
            myApp.pullToRefreshDone();
        }
    });
}


/* ===== Modals Page end pic  ===== */

/* ===== Modals Page lead  ===== */
myApp.onPageInit('lead', function (page) {
    awal = 1;
    lokasi_sekrng = "lead";
    popup_sts = 0;


    myApp.showTab('#tab9');
    $$("#div_filter_done_lead").hide();
    $$('#tab8').on('show', function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_lead").val("").trigger("change");
        $$("#div_filter_done_lead").hide();
    });
    $$('#tab9').on('show', function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_lead").val("").trigger("change");
        $$("#div_filter_done_lead").hide();
    });
    $$('#tab10').on('show', function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_lead").val("").trigger("change");
        $$("#div_filter_done_lead").hide();
    });
    $$('#tab11').on('show', function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_lead").val("").trigger("change");
        $$("#div_filter_done_lead").show();
    });
    $$("#tampil_search_t_lead").on("click", function () {
        myApp.hideNavbar(".navbar");
        myApp.hideToolbar(".tabbar");
    });
    $$("#b_hapus_search_t_lead").on("click", function () {
        myApp.showNavbar(".navbar");
        myApp.showToolbar(".tabbar");
        $$("#search_t_lead").val("").trigger("change");
    });
    $$("#filter_done_lead").on("click", function () {
        myApp.popup(".filter-done-lead");
    });
    var hari_max_flt = new Date();
    hari_max_flt.setDate(hari_max_flt.getDate() - 1);
    var bulan_min_flt = new Date();
    bulan_min_flt.setMonth(bulan_min_flt.getMonth() - 3);
    var calendarDateFormat = myApp.calendar({
        input: '#tgl_mulai_l',
        closeOnSelect: true,
        value: [bulan_min_flt],
        maxDate: [hari_max_flt],
        dateFormat: 'dd MM yyyy'
    });
    var calendarDateFormat2 = myApp.calendar({
        input: '#tgl_akhir_l',
        closeOnSelect: true,
        value: [new Date()],
        dateFormat: 'dd MM yyyy'
    });


    $$("#b_filer_done_leead").on("click", function () {
        $$.ajax({
            url: ipserver + 'lead/div_done.php',
            method: "POST",
            data: {op: "filter_done", id_lead: ls_id, tgl_awal: $$("#tgl_mulai_l").val(), tgl_akhir: $$("#tgl_akhir_l").val()},
            dataType: "json",
            timeout: 5 * 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {
                var isinya = '';
                //   if (a == "pertama") {
                // if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="item-link item-content b_cek_lead" id_sub="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + ' <div class="item-title text_panjang">' + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">PIC : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_lead_tab4").html(isinya);
                    //}

                } else {

                    $$("#div_lead_tab4").html('<li class="list-group-title">Data Not Found</li>');
                }


            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
                myApp.closeModal(".filter-done-lead");
            }
        });
    });
    var ptrContent = $$('.pull-to-refresh-content');
// Add 'refresh' listener on it
    ptrContent.on('refresh', function (e) {
        // Emulate 2s loading
        setTimeout(function () {
            //   alert("mari refresh");
            load_lead_tab4("pertama");
            // myApp.pullToRefreshDone();
        });
    });
    load_lead_tab1("pertama");
});

$$(document).on("click", ".b_cek_lead", function () {
//alert("b_upload_pic"); 
    var id_sub = $$(this).attr("id_sub");
    mainView.router.loadPage('cek_lead.html?id_sub=' + id_sub);
});
var ck_jml_lead_tab1 = 0;
var ck_waktu_lead_tab1 = "";
function load_lead_tab1(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        load_lead_tab2("pertama");
        load_lead_tab3("pertama");
        load_lead_tab4("pertama");
        ck_jml_lead_tab1 = 0;
        ck_waktu_lead_tab1 = "";
        data_post = {id_lead: ls_id, op: "not_done", jumlah: ck_jml_lead_tab1, waktu: ck_waktu_lead_tab1};
    } else {
        data_post = {id_lead: ls_id, op: "not_done", jumlah: ck_jml_lead_tab1, waktu: ck_waktu_lead_tab1};
    }

    $$.ajax({
        url: ipserver + 'lead/div_not_done.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="item-link item-content" id_sub="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + ' <div class="item-title text_panjang">' + kiriman.data[a1][1]
                                + '<br/>'
                                + '<span class="color-green">PIC : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_lead_tab1").html(isinya);
                }

            } else {

                $$("#div_lead_tab1").html('<li class="list-group-title">Data Not Found</li>');
            }


            ck_jml_lead_tab1 = kiriman.jml_data;
            ck_waktu_lead_tab1 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');

        },
        complete: function () {
            //myApp.hideIndicator();

        }
    });
}

var ck_jml_lead_tab2 = 0;
var ck_waktu_lead_tab2 = "";
function load_lead_tab2(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        ck_jml_lead_tab2 = 0;
        ck_waktu_lead_tab2 = "";
        data_post = {id_lead: ls_id, op: "waiting", jumlah: ck_jml_lead_tab2, waktu: ck_waktu_lead_tab2};
    } else {
        data_post = {id_lead: ls_id, op: "waiting", jumlah: ck_jml_lead_tab2, waktu: ck_waktu_lead_tab2};
    }

    $$.ajax({
        url: ipserver + 'lead/div_waiting.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="item-link item-content b_cek_lead" id_sub="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + ' <div class="item-title text_panjang">' + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">PIC : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_lead_tab2").html(isinya);
                }

            } else {

                $$("#div_lead_tab2").html('<li class="list-group-title">Data Not Found</li>');
            }


            ck_jml_lead_tab2 = kiriman.jml_data;
            ck_waktu_lead_tab2 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');

        },
        complete: function () {
            //myApp.hideIndicator();

        }
    });
}

var ck_jml_lead_tab3 = 0;
var ck_waktu_lead_tab3 = "";
function load_lead_tab3(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        ck_jml_lead_tab3 = 0;
        ck_waktu_lead_tab3 = "";
        data_post = {id_lead: ls_id, op: "revisi", jumlah: ck_jml_lead_tab3, waktu: ck_waktu_lead_tab3};
    } else {
        data_post = {id_lead: ls_id, op: "revisi", jumlah: ck_jml_lead_tab3, waktu: ck_waktu_lead_tab3};
    }

    $$.ajax({
        url: ipserver + 'lead/div_revisi.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="item-link item-content b_cek_lead" id_sub="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + ' <div class="item-title text_panjang">' + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">PIC : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_lead_tab3").html(isinya);
                }

            } else {

                $$("#div_lead_tab3").html('<li class="list-group-title">Data Not Found</li>');
            }


            ck_jml_lead_tab3 = kiriman.jml_data;
            ck_waktu_lead_tab3 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');

        },
        complete: function () {
            //myApp.hideIndicator();

        }
    });
}

var ck_jml_lead_tab4 = 0;
var ck_waktu_lead_tab4 = "";
function load_lead_tab4(a) {
    // alert("tess besok");
    var data_post = [];
    if (a == "pertama") {
        ck_jml_lead_tab4 = 0;
        ck_waktu_lead_tab4 = "";
        data_post = {id_lead: ls_id, op: "done", jumlah: ck_jml_lead_tab4, waktu: ck_waktu_lead_tab4};
    } else {
        data_post = {id_lead: ls_id, op: "done", jumlah: ck_jml_lead_tab4, waktu: ck_waktu_lead_tab4};
    }

    $$.ajax({
        url: ipserver + 'lead/div_done.php',
        method: "POST",
        data: data_post,
        dataType: "json",
        timeout: 5 * 60 * 1000,
        beforeSend: function () {
            //myApp.showIndicator();
        },
        success: function (kiriman) {
            var isinya = '';
            //   if (a == "pertama") {
            if (kiriman.jml_data > 0) {
                if (kiriman.cek_baru == "ada") {
                    for (var a1 = 0; a1 < kiriman.data.length; a1++) {

                        //alert(kiriman.data[a1][0]); 
                        isinya += '<li>'
                                + '<a href="#" class="item-link item-content b_cek_lead" id_sub="' + kiriman.data[a1][0] + '">'
                                + '<div class="item-inner">'
                                + ' <div class="item-title text_panjang">' + kiriman.data[a1][1]
                                + '<br/>'
                                + ' <span class="color-green">PIC : ' + kiriman.data[a1][3] + '</span>'
                                + '</div>'
                                + '<div class="item-after">' + kiriman.data[a1][2] + '</div>'
                                + '</div>'
                                + '</a>'
                                + '</li>';
                    }

                    $$("#div_lead_tab4").html(isinya);
                }

            } else {

                $$("#div_lead_tab4").html('<li class="list-group-title">Data Not Found</li>');
            }


            ck_jml_lead_tab4 = kiriman.jml_data;
            ck_waktu_lead_tab4 = kiriman.detik;
        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');
            myApp.pullToRefreshDone();
        },
        complete: function () {
            //myApp.hideIndicator();
            myApp.pullToRefreshDone();
        }
    });
}


/* ===== Modals end Page lead  ===== */


/* ===== Modals Page profil  ===== */
myApp.onPageInit('user', function (page) {
//alert("page user");
    awal = 1;
    lokasi_sekrng = "user";
    $("#f_profil").validate({
        rules: {
            nama: {
                required: true
            },
            email: {
                required: true
            },
            old_pass: {
                required: true
            },
            new_pass: {
                required: true,
                equalTo: "#old_pass"
            }

        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    baca_profil();
    $$("#b_simpan_user").on("click", function () {
        if ($("#f_profil").valid()) {

            var formData = myApp.formToJSON('#f_profil');
            //alert(JSON.stringify(formData));
            //return false;
            $$.ajax({
                url: ipserver + 'user/act_user.php',
                method: "POST",
                data: formData,
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman.status == "ok") {
                        myApp.alert("Save Success ", "Kokola", function () {
                            log_out();
                        });
                    } else {
                        myApp.alert("Failed", "Kokola");
                    }

                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        }
    });
});
function baca_profil() {
    //alert("baca profil");
    $$(".id_user").val(ls_id);
    $$("#nama").val(ls_nama).trigger("change");
    $$("#email").val(ls_email).trigger('change');
}
;
/* ===== Modals end Page profil  ===== */


/* ===== Modals Page edit_anggota  ===== */
myApp.onPageInit('edit_anggota', function (page) {
    lokasi_sekrng = "edit_anggota";
    popup_sts = 0;
    //alert("idm="+page.query.idm+" idl="+page.query.idl+" stss="+page.query.status);
    var tot_kapasitas = 0;
    var tot_peserta_awal = 0;
    var tot_peserta = 0;
    var tot_peserta_ex = 0;

    var waktu_meeting;
    function cek_waktu_meeting(tgl_meeting, id_tempat_meeting) {
        //modiff
        $$.ajax({
            url: ipserver + 'meeting/cek_jam.php',
            method: "POST",
            data: {op: "cek_jam_edit", tgl: tgl_meeting, id_tempat: id_tempat_meeting, id_m: page.query.idm},
            dataType: "json",
            timeout: 3 * 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {

                var misal = kiriman;

                waktu_meeting.destroy();
                waktu_meeting = myApp.picker({
                    input: '#waktu',
                    // rotateEffect: true,
                    formatValue: function (p, values, displayValues) {
                        return values[0] + ' : ' + values[1] + ' until ' + values[2] + ' : ' + values[3];
                    },
                    value: ['00', '00', '00', '00'],
                    cols: [
                        // Hours
                        {
                            values: (function () {
                                var arr = [];
                                for (var i = 0; i <= 23; i++) {
                                    for (var vr = 0; vr < misal.length; vr++) {
                                        if (i == misal[vr][0]) {
                                            for (var ck = misal[vr][0]; ck < misal[vr][2]; ck++) {
                                                i++;

                                            }
                                        }
                                    }
                                    arr.push(i < 10 ? '0' + i : i);
                                }
                                return arr;
                            })(),
                            onChange: function (picker, jam1) {
                                var menit = [];
                                var masuk_ck = '0';

                                //ada di jam2
                                for (var vr = 0; vr < misal.length; vr++) {
                                    //ada di jam2
                                    if (jam1 == misal[vr][2]) {
                                        for (var i = Number(misal[vr][3]); i <= 59; i += 15) {
                                            menit.push(i < 10 ? '0' + i : i);
                                        }
                                        masuk_ck = '1';
                                    }

                                }
                                //tdk ada yg masuk menit
                                if (masuk_ck == '0') {
                                    for (var i = 0; i <= 59; i += 15) {
                                        menit.push(i < 10 ? '0' + i : i);
                                    }
                                }

                                if (picker.cols[2].replaceValues) {
                                    picker.cols[2].replaceValues(menit);
                                }

                                //jam2
                                var jam2 = [];
                                var menit2 = [];
                                var masuk_jam2 = '0';
                                for (var vr = 0; vr < misal.length; vr++) {
                                    //untuk jam2
                                    if (jam1 < misal[vr][0]) {
                                        for (var iw = Number(jam1); iw <= misal[vr][0]; iw++) {
                                            jam2.push(iw < 10 ? '0' + iw : iw);
                                        }
                                        if (picker.cols[4].replaceValues) {
                                            picker.cols[4].replaceValues(jam2);
                                        }

                                        masuk_jam2 = '1';
                                        break;
                                    }


                                }
                                if (masuk_jam2 == '0') {
                                    //untuk jam2
                                    for (var iw = Number(jam1); iw <= 23; iw++) {
                                        jam2.push(iw < 10 ? '0' + iw : iw);
                                    }
                                    if (picker.cols[4].replaceValues) {
                                        picker.cols[4].replaceValues(jam2);
                                    }


                                }

                            }
                        },
                        // Divider
                        {
                            divider: true,
                            content: ':'
                        },
                        // Minutes
                        {
                            values: (function () {
                                var arr = [];
                                for (var i = 0; i <= 59; i += 15) {
                                    arr.push(i < 10 ? '0' + i : i);
                                    if (i == 45) {
                                        arr.push(59);
                                    }
                                }

                                return arr;
                            })(),
                        },
                        // Divider
                        {
                            divider: true,
                            content: 'until'
                        },
                        // Hours
                        {
                            values: (function () {
                                var arr = [];
                                for (var i = 0; i <= 23; i++) {
                                    arr.push(i < 10 ? '0' + i : i);
                                }
                                return arr;
                            })(),
                            onChange: function (picker, jam2) {
                                var menit = [];
                                var masuk_ck = '0';

                                for (var vr = 0; vr < misal.length; vr++) {
                                    //untuk jam2
                                    if (jam2 == misal[vr][0]) {
                                        for (var iw = 0; iw <= misal[vr][1]; iw += 15) {
                                            menit.push(iw < 10 ? '0' + iw : iw);
                                        }
                                        if (picker.cols[6].replaceValues) {
                                            picker.cols[6].replaceValues(menit);
                                        }

                                        masuk_ck = '1';
                                    }
                                }

                                if (masuk_ck == '0') {
                                    for (var iw = 0; iw <= 59; iw += 15) {
                                        menit.push(iw < 10 ? '0' + iw : iw);
                                    }
                                    if (picker.cols[6].replaceValues) {
                                        picker.cols[6].replaceValues(menit);
                                    }
                                }


                            }
                        },
                        // Divider
                        {
                            divider: true,
                            content: ':'
                        },
                        // Minutes
                        {
                            values: (function () {
                                var arr = [];
                                for (var i = 0; i <= 59; i += 15) {
                                    arr.push(i < 10 ? '0' + i : i);

                                }
                                return arr;
                            })(),
                        }
                    ],
                    onClose: function () {
                        // alert("iki ke sekian");
                        baca_anggota_blm_daftar_new($$("#tanggal").val(), $$("#waktu").val(), "destroy");
                    }
                });
                //end modif
            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
            }
        });
    }

    var ck_tgl_awal = 0;
    $$("#tanggal").on("change", function () {
        //alert($$("#tanggal").val());
        if (ck_tgl_awal != 0) {
            baca_anggota_blm_daftar_new($$("#tanggal").val(), $$("#waktu").val(), "destroy");
        }
        ck_tgl_awal = 1;

        if ($$("#tempat").val() != "" && $$("#tempat").val() != null) {
            cek_waktu_meeting($$("#tanggal").val(), $$('#id_tempat').val());
        }

    });

    var anggota_terdaftar;


    function baca_anggota_blm_daftar_new(tgl, wak, sts) {
        myApp.showIndicator();

        $$.post(ipserver + "meeting/act_meeting.php",
                {op: "baca_anggota_belum_terdaftar_V1.09", id_meet: page.query.idm, idl: page.query.idl, tgl_m: tgl, waktu: wak},
        function (datanya) {
            myApp.hideIndicator();
            var data_auto_ajax = $.parseJSON(datanya);

            var data_dipilih = [];
            var tmpng_nama = "";

            if (sts == "destroy") {
                anggota_terdaftar.destroy();
            }

            var id_terpilih = $$('#id_anggota').val().split(',');
            //alert(id_terpilih.length);

            $.each(data_auto_ajax, function (indx, value) {
                for (var i = 0; i < id_terpilih.length; i++) {
                    if (id_terpilih[i] == value.id) {
                        data_dipilih.push({id: value.id, name: value.name});
                        if (tmpng_nama == "") {
                            tmpng_nama += value.name;
                        } else {
                            tmpng_nama += ", " + value.name;
                        }

                    }
                }
            });


            // $$('#anggota').val(tmpng_nama).trigger('change');
            if (tmpng_nama == "") {
                $$('#anggota_nw').html('<span class="color-gray">Add Participants</span>');
            } else {
                $$('#anggota_nw').html(tmpng_nama);
            }


            anggota_terdaftar = myApp.autocomplete({
                openIn: 'popup', //open in page
                opener: $$('#anggota_nw'), //link that opens autocomplete
                multiple: true, //allow multiple values
                valueProperty: 'id', //object's "value" property name
                textProperty: 'name', //object's "text" property name
                searchbarPlaceholderText: "Search...",
                value: data_dipilih,
                pageTitle: "Choose Participants",
                // limit: 20, //limit to 20 results
                source: function (autocomplete, query, render) {
                    var results = [];
                    // Find matched items
                    for (var i = 0; i < data_auto_ajax.length; i++) {
                        if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                            results.push(data_auto_ajax[i]);
                    }
                    // Render items by passing array with result items
                    render(results);
                },
                onChange: function (autocomplete, value) {
                    //alert(JSON.stringify(value));
                    //alert(value[0].id);
                    var id_ctt = "";
                    var value2 = "";
                    tot_peserta = 0;

                    for (var i = 0; i < value.length; i++) {
                        if (i == 0) {
                            value2 += value[i].name;
                            id_ctt += value[i].id;
                        } else {
                            value2 += ", " + value[i].name;
                            id_ctt += "," + value[i].id;
                        }
                        tot_peserta++;
                    }

                    //$$('#anggota').val(value2).trigger('change');
                    if (value2 == "") {
                        $$('#anggota_nw').html('<span class="color-gray">Add Participants</span>');
                    } else {
                        $$('#anggota_nw').html(value2);
                    }

                    $$('#id_anggota').val(id_ctt);
                },
                onOpen: function () {
                    //if ($$('#anggota').val() == "") {
                    if ($$('#anggota_nw').text() == "Add Participants") {
                        $$(".searchbar-input input").val("a").trigger("change");
                        setTimeout(function () {
                            $$(".searchbar-input input").val("").trigger("change");
                        }, 10);
                    }
                    popup_sts = 1;
                },
                onClose: function () {
                    popup_sts = 0;
                }
            });
        });
    }

    $$(".eb_sel_all").on("click", function () {
//            alert("semua");
        myApp.showIndicator();
        $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_belum_terdaftar_V1.09", id_meet: page.query.idm, idl: page.query.idl, tgl_m: $$("#tanggal").val(), waktu: $$("#waktu").val()}, function (datanya) {
            var data_auto_ajax = $.parseJSON(datanya);

            myApp.hideIndicator();

            var id_cttp = "";
            var value2p = "";
            tot_peserta = 0;

            var ip = 0;

            $.each(data_auto_ajax, function (ind, value) {

                if (ip == 0) {
                    value2p += value.name;
                    id_cttp += value.id;
                } else {
                    value2p += ", " + value.name;
                    id_cttp += "," + value.id;
                }
                tot_peserta++;
                ip++;
            });

            //$$('#anggota').val(value2p).trigger('change');
            if (value2p == "") {
                $$('#anggota_nw').html('<span class="color-gray">Add Participants</span>');
            } else {
                $$('#anggota_nw').html(value2p);
            }

            $$('#id_anggota').val(id_cttp);

            anggota_terdaftar.destroy();
            anggota_terdaftar = myApp.autocomplete({
                openIn: 'popup', //open in page
                opener: $$('#anggota_nw'), //link that opens autocomplete
                multiple: true, //allow multiple values
                valueProperty: 'id', //object's "value" property name
                textProperty: 'name', //object's "text" property name
                searchbarPlaceholderText: "Search...",
                pageTitle: "Choose Participants",
                value: data_auto_ajax,
                // limit: 20, //limit to 20 results
                source: function (autocomplete, query, render) {
                    var results = [];
                    // Find matched items
                    for (var i = 0; i < data_auto_ajax.length; i++) {
                        if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                            results.push(data_auto_ajax[i]);
                    }
                    // Render items by passing array with result items
                    render(results);
                },
                onChange: function (autocomplete, value) {
                    //alert(JSON.stringify(value));
                    //alert(value[0].id);
                    var id_ctt = "";
                    var value2 = "";
                    tot_peserta = 0;

                    for (var i = 0; i < value.length; i++) {
                        if (i == 0) {
                            value2 += value[i].name;
                            id_ctt += value[i].id;
                        } else {
                            value2 += ", " + value[i].name;
                            id_ctt += "," + value[i].id;
                        }
                        tot_peserta++;
                    }

                    //$$('#anggota').val(value2).trigger('change');
                    if (value2 == "") {
                        $$('#anggota_nw').html('<span class="color-gray">Add Participants</span>');
                    } else {
                        $$('#anggota_nw').html(value2);
                    }

                    $$('#id_anggota').val(id_ctt);
                },
                onOpen: function () {
                    if ($$('#anggota_nw').text() == "Add Participants") {
                        $$(".searchbar-input input").val("a").trigger("change");
                        setTimeout(function () {
                            $$(".searchbar-input input").val("").trigger("change");
                        }, 10);
                    }
                    popup_sts = 1;
                },
                onClose: function () {
                    popup_sts = 0;
                }
            });
        });

        myApp.swipeoutClose(".k_ang");

    });


    $$(".eb_des_all").on("click", function () {
        myApp.showIndicator();

        $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_belum_terdaftar_V1.09", id_meet: page.query.idm, idl: page.query.idl, tgl_m: $$("#tanggal").val(), waktu: $$("#waktu").val()}, function (datanya) {
            var data_auto_ajax = $.parseJSON(datanya);
            myApp.hideIndicator();

            //$$('#anggota').val("").trigger('change');
            $$('#anggota_nw').html('<span class="color-gray">Add Participants</span>');

            $$('#id_anggota').val("");

            anggota_terdaftar.destroy();
            anggota_terdaftar = myApp.autocomplete({
                openIn: 'popup', //open in page
                opener: $$('#anggota_nw'), //link that opens autocomplete
                multiple: true, //allow multiple values
                valueProperty: 'id', //object's "value" property name
                textProperty: 'name', //object's "text" property name
                searchbarPlaceholderText: "Search...",
                pageTitle: "Choose Participants",
                // limit: 20, //limit to 20 results
                source: function (autocomplete, query, render) {
                    var results = [];
                    // Find matched items
                    for (var i = 0; i < data_auto_ajax.length; i++) {
                        if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                            results.push(data_auto_ajax[i]);
                    }
                    // Render items by passing array with result items
                    render(results);
                },
                onChange: function (autocomplete, value) {
                    //alert(JSON.stringify(value));
                    //alert(value[0].id);
                    var id_ctt = "";
                    var value2 = "";
                    tot_peserta = 0;

                    for (var i = 0; i < value.length; i++) {
                        if (i == 0) {
                            value2 += value[i].name;
                            id_ctt += value[i].id;
                        } else {
                            value2 += ", " + value[i].name;
                            id_ctt += "," + value[i].id;
                        }
                        tot_peserta++;
                    }

                    //$$('#anggota').val(value2).trigger('change');
                    if (value2 == "") {
                        $$('#anggota_nw').html('<span class="color-gray">Add Participants</span>');
                    } else {
                        $$('#anggota_nw').html(value2);
                    }

                    $$('#id_anggota').val(id_ctt);
                },
                onOpen: function () {
                    if ($$('#anggota_nw').text() == "Add Participants") {
                        $$(".searchbar-input input").val("a").trigger("change");
                        setTimeout(function () {
                            $$(".searchbar-input input").val("").trigger("change");
                        }, 10);
                    }
                    popup_sts = 1;
                },
                onClose: function () {
                    popup_sts = 0;
                }
            });
        });

        myApp.swipeoutClose(".k_ang");
    });

    var tempat;
    $$.ajax({
        url: ipserver + 'meeting/act_meeting.php',
        method: "POST",
        data: {op: "edit_data_meeting", id_meet: page.query.idm},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {
            tot_peserta_awal = kiriman[10];

            $$("#topic").val(kiriman[4]).trigger("change");
            $$("#tools").val(kiriman[5]).trigger("change");
            var hari_mulai = new Date();
            hari_mulai.setDate(hari_mulai.getDate() - 1);
            var calendarDateFormat2 = myApp.calendar({
                closeOnSelect: true,
                input: '#tanggal',
                value: [kiriman[0]],
                minDate: hari_mulai,
                dateFormat: 'dd MM yyyy'
            });

            $$.ajax({
                url: ipserver + 'meeting/cek_jam.php',
                method: "POST",
                data: {op: "cek_jam_edit", tgl: $$("#tanggal").val(), id_tempat: kiriman[3], id_m: page.query.idm},
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman_c) {

                    var misal = kiriman_c;

                    waktu_meeting = myApp.picker({
                        input: '#waktu',
                        // rotateEffect: true,
                        formatValue: function (p, values, displayValues) {
                            return values[0] + ' : ' + values[1] + ' until ' + values[2] + ' : ' + values[3];
                        },
                        value: [kiriman[1], kiriman[2], kiriman[6], kiriman[7]],
                        cols: [
                            // Hours
                            {
                                values: (function () {
                                    var arr = [];
                                    for (var i = 0; i <= 23; i++) {
                                        for (var vr = 0; vr < misal.length; vr++) {
                                            if (i == misal[vr][0]) {
                                                for (var ck = misal[vr][0]; ck < misal[vr][2]; ck++) {
                                                    i++;

                                                }
                                            }
                                        }
                                        arr.push(i < 10 ? '0' + i : i);
                                    }
                                    return arr;
                                })(),
                                onChange: function (picker, jam1) {
                                    var menit = [];
                                    var masuk_ck = '0';

                                    //ada di jam2
                                    for (var vr = 0; vr < misal.length; vr++) {
                                        //ada di jam2
                                        if (jam1 == misal[vr][2]) {
                                            for (var i = Number(misal[vr][3]); i <= 59; i += 15) {
                                                menit.push(i < 10 ? '0' + i : i);
                                            }
                                            masuk_ck = '1';
                                        }

                                    }
                                    //tdk ada yg masuk menit
                                    if (masuk_ck == '0') {
                                        for (var i = 0; i <= 59; i += 15) {
                                            menit.push(i < 10 ? '0' + i : i);
                                        }
                                    }

                                    if (picker.cols[2].replaceValues) {
                                        picker.cols[2].replaceValues(menit);
                                    }

                                    //jam2
                                    var jam2 = [];
                                    var menit2 = [];
                                    var masuk_jam2 = '0';
                                    for (var vr = 0; vr < misal.length; vr++) {
                                        //untuk jam2
                                        if (jam1 < misal[vr][0]) {
                                            for (var iw = Number(jam1); iw <= misal[vr][0]; iw++) {
                                                jam2.push(iw < 10 ? '0' + iw : iw);
                                            }
                                            if (picker.cols[4].replaceValues) {
                                                picker.cols[4].replaceValues(jam2);
                                            }

                                            masuk_jam2 = '1';
                                            break;
                                        }


                                    }
                                    if (masuk_jam2 == '0') {
                                        //untuk jam2
                                        for (var iw = Number(jam1); iw <= 23; iw++) {
                                            jam2.push(iw < 10 ? '0' + iw : iw);
                                        }
                                        if (picker.cols[4].replaceValues) {
                                            picker.cols[4].replaceValues(jam2);
                                        }


                                    }

                                }
                            },
                            // Divider
                            {
                                divider: true,
                                content: ':'
                            },
                            // Minutes
                            {
                                values: (function () {
                                    var arr = [];
                                    for (var i = 0; i <= 59; i += 15) {
                                        arr.push(i < 10 ? '0' + i : i);
                                        if (i == 45) {
                                            arr.push(59);
                                        }
                                    }

                                    return arr;
                                })(),
                            },
                            // Divider
                            {
                                divider: true,
                                content: 'until'
                            },
                            // Hours
                            {
                                values: (function () {
                                    var arr = [];
                                    for (var i = 0; i <= 23; i++) {
                                        arr.push(i < 10 ? '0' + i : i);
                                    }
                                    return arr;
                                })(),
                                onChange: function (picker, jam2) {
                                    var menit = [];
                                    var masuk_ck = '0';

                                    for (var vr = 0; vr < misal.length; vr++) {
                                        //untuk jam2
                                        if (jam2 == misal[vr][0]) {
                                            for (var iw = 0; iw <= misal[vr][1]; iw += 15) {
                                                menit.push(iw < 10 ? '0' + iw : iw);
                                            }
                                            if (picker.cols[6].replaceValues) {
                                                picker.cols[6].replaceValues(menit);
                                            }

                                            masuk_ck = '1';
                                        }
                                    }

                                    if (masuk_ck == '0') {
                                        for (var iw = 0; iw <= 59; iw += 15) {
                                            menit.push(iw < 10 ? '0' + iw : iw);
                                        }
                                        if (picker.cols[6].replaceValues) {
                                            picker.cols[6].replaceValues(menit);
                                        }
                                    }


                                }
                            },
                            // Divider
                            {
                                divider: true,
                                content: ':'
                            },
                            // Minutes
                            {
                                values: (function () {
                                    var arr = [];
                                    for (var i = 0; i <= 59; i += 15) {
                                        arr.push(i < 10 ? '0' + i : i);

                                    }
                                    return arr;
                                })(),
                            }
                        ],
                        onClose: function () {
                            // alert("iki awal");
                            baca_anggota_blm_daftar_new($$("#tanggal").val(), $$("#waktu").val(), "destroy");
                        }
                    });
                    //end modif

                    baca_anggota_blm_daftar_new($$("#tanggal").val(), $$("#waktu").val(), "awal");
                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });


            $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_tempat"}, function (datanya) {
                var data_auto_ajax = $.parseJSON(datanya);

                tot_kapasitas = kiriman[9];
                $$('#id_tempat').val(kiriman[3]);

                if (kiriman[9] != '1000') {
                    $$("#tempat").val(kiriman[8] + " (" + kiriman[9] + ")").trigger("change");
                } else {
                    $$("#tempat").val(kiriman[8]).trigger("change");
                }

                tempat = myApp.autocomplete({
                    openIn: 'popup', //open in page
                    opener: $$('#tempat'), //link that opens autocomplete
                    preloader: true, //enable preloader
                    valueProperty: 'id', //object's "value" property name
                    textProperty: 'name', //object's "text" property name
                    value: [{"id": kiriman[3], "name": kiriman[8]}],
                    backOnSelect: true, // expand input
                    searchbarPlaceholderText: "Search...",
                    pageTitle: "Choose Place",
                    //  limit: 20, //limit to 20 results
                    source: function (autocomplete, query, render) {
                        var results = [];
                        // Find matched items
                        for (var i = 0; i < data_auto_ajax.length; i++) {
                            if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                                results.push(data_auto_ajax[i]);
                        }
                        // Render items by passing array with result items
                        render(results);
                    },
                    onChange: function (autocomplete, value) {
                        //alert(value[0].id);
                        $$('#tempat').val(value[0].name).trigger('change');
                        $$('#id_tempat').val(value[0].id);
                        tot_kapasitas = value[0].kapasitas;

                        //cek waktu lagi
                        cek_waktu_meeting($$("#tanggal").val(), value[0].id);
                    },
                    onOpen: function () {

                        $$(".searchbar-input input").val("a").trigger("change");
                        setTimeout(function () {
                            $$(".searchbar-input input").val("").trigger("change");
                        }, 10);

                        popup_sts = 1;
                    },
                    onClose: function () {
                        popup_sts = 0;
                    }

                });
            });
        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
    $$(".b_ke_edit_notulen").on("click", function () {
        mainView.router.loadPage('edit_notulen.html?idm=' + page.query.idm
                + '&idl=' + page.query.idl
                + '&status=' + page.query.status);
    });
    $("#f_edit_anggota").validate({
        rules: {
            "anggota2[]": {
                email: true
            },
            tanggal: {
                required: true
            },
            waktu: {
                required: true
            },
            tempat: {
                required: true
            },
            topic: {
                required: true
            },
            tools: {
                required: true
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    $$("#b_simpan").on("click", function () {
        if ($("#f_edit_anggota").valid()) {
            tot_peserta_ex = 0;
            $$("input[name*='anggota2[]']").each(function () {
                if ($$(this).val() != "") {
                    tot_peserta_ex++;
                }
            });
            if ((Number(tot_peserta_awal) + Number(tot_peserta) + Number(tot_peserta_ex)) <= tot_kapasitas) {
                $$(".id_meet_hidden").val(page.query.idm);
                var form = $$("#f_edit_anggota")[0];
                var formData = new FormData(form);
                $$.ajax({
                    url: ipserver + 'meeting/act_meeting.php',
                    method: "POST",
                    data: formData,
                    dataType: "json",
                    timeout: 3 * 60 * 1000,
                    beforeSend: function () {
                        myApp.showIndicator();
                    },
                    success: function (kiriman) {
                        if (kiriman == "ok") {
                            myApp.alert("Save Success ", "Kokola", function () {
                                mainView.router.loadPage('edit_notulen.html?idm=' + page.query.idm
                                        + '&idl=' + page.query.idl
                                        + '&status=' + page.query.status);
                            });
                        } else if (kiriman == "pesan") {
                            myApp.alert("Time has been used, please change the time", "Kokola",
                                    function () {
                                        cek_waktu_meeting($$("#tanggal").val(), $$('#id_tempat').val());
                                    });
                        } else {
                            myApp.alert("Failed", "Kokola");
                        }

                    },
                    error: function (data) {
                        myApp.alert('No Connection or Error', 'Kokola');
                        myApp.hideIndicator();
                    },
                    complete: function () {
                        myApp.hideIndicator();
                    }
                });

            } else {
                myApp.alert("The number of participants exceeds "
                        + "the capacity of the room."
                        + "<br/>Total capacity = " + tot_kapasitas
                        + "<br/>Total participants = " + (Number(tot_peserta_awal) + Number(tot_peserta) + Number(tot_peserta_ex)), "Kokola");

            }
        }


    });
    $$("#tambah_anggota").on("click", function () {
        //alert("tes tambah"); 
        var isinya = "";
        isinya = '<li class="swipeout">'
                + '<div class="swipeout-content item-content">'
                + '<div class="item-media delete_anggota"><i class="fa fa-users fa-2x color-brown"></i></div>'
                + '<div class="item-inner">'
                + '<div class="item-title floating-label">Anggota Eksternal (E-mail)</div>'
                + '<div class="item-input">'
                + '<input type="email" name="anggota2[]" placeholder=""/>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<div class="swipeout-actions-right">'
                + '<a href="#" class="swipeout-delete" data-confirm="Are you sure want to delete?" data-confirm-title="Delete?" data-close-on-cancel="true">Delete</a>'
                + '</div>'
                + '</div>'
                + '</li>';
        $$("#f_edit_anggota").append(isinya);
    });

    $$(".b_tambah_tempat").on("click", function () {
        $$("#tempat_baru").val("").trigger("change");
        myApp.popup(".popup-tambah_tempat");
    });

    $("#f_tambah_tempat").validate({
        rules: {
            tempat_baru: {
                required: true,
                remote: ipserver + "meeting/cek_nama_tempat.php"
            }
        },
        messages: {
            tempat_baru: {
                remote: "Place already exists"
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    $$("#b_simpan_tambah_tempat").on("click", function () {
        if ($("#f_tambah_tempat").valid()) {
            //alert("b_simpan_tambah_tempat");
            $$.ajax({
                url: ipserver + 'meeting/act_meeting.php',
                method: "POST",
                data: {op: "tambah_tampat_meeting", nama_tempat: $$("#tempat_baru").val()},
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "ok") {
                        myApp.alert("Save Success ", "Kokola", function () {
                            $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_tempat"}, function (datanya) {
                                var data_auto_ajax = $.parseJSON(datanya);

                                tempat.destroy();
                                tempat = myApp.autocomplete({
                                    openIn: 'popup', //open in page
                                    opener: $$('#tempat'), //link that opens autocomplete
                                    preloader: true, //enable preloader
                                    valueProperty: 'id', //object's "value" property name
                                    textProperty: 'name', //object's "text" property name
                                    value: [{"id": kiriman[3], "name": kiriman[8]}],
                                    backOnSelect: true, // expand input
                                    searchbarPlaceholderText: "Search...",
                                    pageTitle: "Choose Place",
                                    //  limit: 20, //limit to 20 results
                                    source: function (autocomplete, query, render) {
                                        var results = [];
                                        // Find matched items
                                        for (var i = 0; i < data_auto_ajax.length; i++) {
                                            if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                                                results.push(data_auto_ajax[i]);
                                        }
                                        // Render items by passing array with result items
                                        render(results);
                                    },
                                    onChange: function (autocomplete, value) {
                                        //alert(value[0].id);
                                        $$('#tempat').val(value[0].name).trigger('change');
                                        $$('#id_tempat').val(value[0].id);
                                        tot_kapasitas = value[0].kapasitas;
                                    },
                                    onOpen: function () {

                                        $$(".searchbar-input input").val("a").trigger("change");
                                        setTimeout(function () {
                                            $$(".searchbar-input input").val("").trigger("change");
                                        }, 10);

                                        popup_sts = 1;
                                    },
                                    onClose: function () {
                                        popup_sts = 0;
                                    }

                                });

                                myApp.hideIndicator();
                                myApp.closeModal(".popup-tambah_tempat");
                            });

                        });

                    } else {
                        myApp.alert("Failed", "Kokola");
                    }
                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    //myApp.hideIndicator();
                }
            });
        }
    });


});
/* ===== Modals Page end edit_anggota  ===== */


/* ===== Modals Page edit_subtopic  ===== */
myApp.onPageInit('edit_subtopic', function (page) {
    popup_sts = 0;
    lokasi_sekrng = "edit_subtopic";
//    alert("id_sub="+page.query.id_sub
//            +" idmeet"+page.query.idm
//            +" idlead"+page.query.idl
//            +" status"+page.query.status);



    $$.ajax({
        url: ipserver + "meeting/act_submeeting.php",
        method: "POST",
        data: {op: "baca_edit_subtopic", id_sub: page.query.id_sub},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {

            $$(".id_subtopic_hidden").val(kiriman[0]);

            tinymce.remove('.tiny');
            tinymce.init({
                selector: '.tiny',
                menubar: false,
                statusbar: false
                        //toolbar: 'undo redo | styleselect | bold italic | link image'
            });

            tinymce.get('subtopic3').setContent(kiriman[1]);


            $$("#pic3").val(kiriman[3]).trigger("change");
            $$("#duedate3").val(kiriman[4]).trigger("change");
            $$("#keterangan3").val(kiriman[5]).trigger("change");
            $$("#id_pic3").val(kiriman[2]);

            $("input[name=kategori][value='" + kiriman[6] + "']").prop("checked", true);

            var hari_mulai = new Date();
            hari_mulai.setDate(hari_mulai.getDate() - 1);
            var calendarDateFormat2 = myApp.calendar({
                closeOnSelect: true,
                input: '#duedate3',
                value: [kiriman[4]],
                minDate: hari_mulai,
                dateFormat: 'dd MM yyyy'
            });
            $$.post(ipserver + "meeting/act_meeting.php", {op: "baca_anggota_terdaftar_user", id_meet: page.query.idm}, function (datanya) {

                var data_auto_ajax = $.parseJSON(datanya);
                data_auto_ajax.push({id: ls_id, name: ls_nama});

                var pic2 = myApp.autocomplete({
                    openIn: 'page', //open in page
                    opener: $$('#pic3'), //link that opens autocomplete
                    preloader: true, //enable preloader
                    valueProperty: 'id', //object's "value" property name
                    textProperty: 'name', //object's "text" property name
                    backOnSelect: true, // expand input
                    value: [{"id": kiriman[2], "name": kiriman[3]}],
                    searchbarPlaceholderText: "Search...",
                    pageTitle: "Choose PIC",
                    source: function (autocomplete, query, render) {
                        var results = [];
                        // Find matched items
                        for (var i = 0; i < data_auto_ajax.length; i++) {
                            if (data_auto_ajax[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
                                results.push(data_auto_ajax[i]);
                        }
                        // Render items by passing array with result items
                        render(results);
                    },
                    onChange: function (autocomplete, value) {
                        //alert(value[0].id);
                        $$('#pic3').val(value[0].name).trigger('change');
                        $$('#id_pic3').val(value[0].id);
                    },
                    onOpen: function () {
                        $$(".searchbar-input input").val("a").trigger("change");
                        setTimeout(function () {
                            $$(".searchbar-input input").val("").trigger("change");
                        }, 10);
                        popup_sts = 3;
                    },
                    onClose: function () {
                        popup_sts = 1;
                    }

                });
            });
        },
        error: function () {
            alert("No Connection");
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
    $$(".b_ke_edit_notulen").on("click", function () {
        mainView.router.loadPage('edit_notulen.html?idm=' + page.query.idm
                + '&idl=' + page.query.idl
                + '&status=' + page.query.status);
    });
    $("#f_edit_subtopic").validate({
        rules: {
            subtopic3: {
                required: true
            },
            pic3: {
                required: true
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
    $$("#b_simpan").on("click", function () {
        //alert(":asa save"); 
        if ($("#f_edit_subtopic").valid()) {
            tinymce.triggerSave();
            var form = $$("#f_edit_subtopic")[0];
            var formData = new FormData(form);
            $$.ajax({
                url: ipserver + 'meeting/act_submeeting.php',
                method: "POST",
                data: formData,
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "ok") {
                        myApp.alert("Save Success ", "Kokola", function () {


                            mainView.router.loadPage('edit_notulen.html?idm=' + page.query.idm
                                    + '&idl=' + page.query.idl
                                    + '&status=' + page.query.status);
                        });
                    } else {
                        myApp.alert("Failed", "Kokola");
                    }

                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        }
    });
});
$$(document).on("click", ".b_hapus_subtopic", function () {
//alert("hapuss"+$$(this).attr("unix_id"));        
//myApp.swipeoutDelete($$(this).parents(".swipeout"));
    var ini_ya = $$(this);
    myApp.confirm('Are you sure want to delete this item?', 'Kokola', function () {
        $$.ajax({
            url: ipserver + 'meeting/act_submeeting.php',
            method: "POST",
            data: {op: "delete_subtopic", id_sub: ini_ya.attr("unix_id")},
            dataType: "json",
            timeout: 3 * 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {
                if (kiriman == "ok") {
                    myApp.swipeoutDelete(ini_ya.parents(".swipeout"));
                } else {
                    myApp.alert("Failed", "Kokola");
                }

            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
            }
        });
    });
});
/* ===== Modals Page end edit_subtopic  ===== */

/* ===== Modals Page upload_pic  ===== */
myApp.onPageInit('upload_pic', function (page) {
    popup_sts = 0;
    lokasi_sekrng = "upload_pic";
    // alert(page.query.id_subtopic);
    $$(".id_subtopic_pic").val(page.query.id_subtopic);
    $$.ajax({
        url: ipserver + 'pic/act_pic.php',
        method: "POST",
        data: {op: "baca_data_upic", id_sub: page.query.id_subtopic},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {

            $$(".txt_subtopic").html(kiriman[8]);
            $$(".txt_nama_lead").text(kiriman[0]);
            $$(".txt_tgl_meet").text(kiriman[1]);
            $$(".txt_waktu_meet").text(kiriman[2]);
            $$(".txt_tempat_meet").text(kiriman[3]);
            $$(".txt_topic_meet").text(kiriman[4]);
            $$(".txt_tools_meet").text(kiriman[5]);
            $$(".txt_nama_pic").text(kiriman[6]);
            $$(".txt_ddate").text(kiriman[7]);
            if (kiriman[9] != "" && kiriman[9] != null) {
                $$("#catatan").val(kiriman[9]).trigger("change");
            }

            if (kiriman[10] == "") {
                $$(".kategori").text("-");
            } else {
                $$(".kategori").text(kiriman[10]);
            }

        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
    $.validator.addMethod('filesize', function (value, element, param) {

        return this.optional(element) || (element.files[0].size <= param)
    });
    $("#f_upload_pic").validate({
        rules: {
            u_pic: {
                required: true,
                filesize: (1024 * 1024) * 2
            }

        },
        messages: {
            u_pic: {
                filesize: "File must be less than 2MB"
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parents(".up_pic"));
        }
    });

    $$("#u_pic").on("change", function () {
        if ($("#f_upload_pic").valid()) {
            //alert("tes");
            var form = $$("#f_upload_pic")[0];
            var formData = new FormData(form);
            $$.ajax({
                url: ipserver + 'pic/act_pic.php',
                method: "POST",
                data: formData,
                dataType: "json",
                timeout: 10 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "sukses") {
                        $$("#f_upload_pic")[0].reset();
                        baca_list_upload(page.query.id_subtopic);
                        myApp.alert("Upload Succes", "Kokola");
                    } else {
                        myApp.alert("Upload Failed", "Kokola");
                    }
                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        }


    });
    $$("#b_simpan_upload_pic").on("click", function () {
        //alert("save");
        myApp.confirm("Are you sure want to Finish?", "Kokola", function () {
            $$.ajax({
                url: ipserver + 'pic/act_pic.php',
                method: "POST",
                data: {op: "finish_pic", id_sub: page.query.id_subtopic, note: $$("#catatan").val()},
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "ok") {
                        myApp.alert("Save Success", "Kokola");
                        mainView.router.loadPage("pic.html");
                    } else {
                        myApp.alert("Failed", "Kokola");
                    }

                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        });
    });
    baca_list_upload(page.query.id_subtopic);
    $$("#b_back").on("click", function () {
        mainView.router.loadPage("pic.html");
    });
});
function baca_list_upload(id_sub) {

    $$.ajax({
        url: ipserver + 'pic/act_pic.php',
        method: "POST",
        data: {op: "baca_list_file", sub_topic: id_sub},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {
            var isi = '';
            for (var ind = 0; ind < kiriman.data.length; ind++) {
                isi += '<li class="swipeout">'
                        + '<div class="swipeout-content item-content b_download_upic" url_down="' + kiriman.data[ind][3] + '" nama-file="' + kiriman.data[ind][2] + '">'
                        + ' <div class="item-media"><i class="fa fa-file-text-o fa-2x color-indigo"></i></div>'
                        + '<div class="item-inner"> '
                        + kiriman.data[ind][2]
                        + '</div>'
                        + '</div>'
                        + '<div class="swipeout-actions-right">'
                        + '<a href="#" class="swipeout bg-red delete_u_pic" d-idsub="' + kiriman.data[ind][0] + '" '
                        + 'd-url="' + kiriman.data[ind][1] + '">Delete</a>'
                        + '</div>'
                        + '</li>';
            }
            //$$(isi).insertAfter("#tempat_list_file");
            $$("#div_upload_pic").html(isi);
        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
}

function baca_list_upload_saja(id_sub) {

    $$.ajax({
        url: ipserver + 'pic/act_pic.php',
        method: "POST",
        data: {op: "baca_list_file", sub_topic: id_sub},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {
            var isi = '';
            for (var ind = 0; ind < kiriman.data.length; ind++) {
                isi += '<li class="swipeout">'
                        + '<div class="swipeout-content item-content b_download_upic_saja" url_down="' + kiriman.data[ind][3] + '" nama-file="' + kiriman.data[ind][2] + '">'
                        + ' <div class="item-media"><i class="fa fa-file-text-o fa-2x color-indigo"></i></div>'
                        + '<div class="item-inner"> '
                        + kiriman.data[ind][2]
                        + '</div>'
                        + '</div>'
                        + '</li>';
            }
            //$$(isi).insertAfter("#tempat_list_file");
            $$("#div_upload_pic").html(isi);
        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
}

function baca_list_uploadrevisi_saja(id_sub) {

    $$.ajax({
        url: ipserver + 'lead/act_lead.php',
        method: "POST",
        data: {op: "baca_list_file", sub_topic: id_sub},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {
            var isi = '';
            for (var ind = 0; ind < kiriman.data.length; ind++) {

                if (kiriman.data[ind][1] != "" && kiriman.data[ind][1] != null) {
                    isi += '<li class="swipeout">'
                            + '<div class="swipeout-content item-content b_download_upic_saja" url_down="' + kiriman.data[ind][0] + '" nama-file="' + kiriman.data[ind][1] + '">'
                            + ' <div class="item-media"><i class="fa fa-file-text-o fa-2x color-red"></i></div>'
                            + '<div class="item-inner"> '
                            + kiriman.data[ind][1]
                            + '</div>'
                            + '</div>'
                            + '</li>';
                }
            }
            //$$(isi).insertAfter("#tempat_list_file");
            $$("#div_upload_lead_rev").html(isi);
        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
}

$$(document).on("click", ".delete_u_pic", function () {
    var ini_ya = $$(this);
    var id_upload = ini_ya.attr("d-idsub");
    var urlnya = ini_ya.attr("d-url");
    myApp.confirm('Are you sure want to delete this item?', 'Kokola', function () {
        $$.ajax({
            url: ipserver + 'pic/act_pic.php',
            method: "POST",
            data: {op: "delete_u_pic", id_up: id_upload, url: urlnya},
            dataType: "json", timeout: 3 * 60 * 1000,
            beforeSend: function () {
                myApp.showIndicator();
            },
            success: function (kiriman) {
                if (kiriman == "ok") {
                    myApp.swipeoutDelete(ini_ya.parents(".swipeout"));
                } else {
                    myApp.alert("Failed", "Kokola");
                }

            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                myApp.hideIndicator();
            },
            complete: function () {
                myApp.hideIndicator();
            }
        });
    });
});
$$(document).on("taphold", ".b_download_upic", function () {

    var urliki = $$(this).attr("url_down");
    var nama_file = $$(this).attr("nama-file");
    myApp.confirm("Open File " + nama_file, "Kokola", function () {
        document.addEventListener('deviceready', function () {

            //downloadFile(urliki, nama_file);
            window.open = cordova.InAppBrowser.open;
            window.open(encodeURI(urliki), '_system', 'location=no');
        }, false);
    });
});
$$(document).on("click", ".b_download_upic_saja", function () {
    var urliki = $$(this).attr("url_down");
    var nama_file = $$(this).attr("nama-file");
    myApp.confirm("Open File " + nama_file, "Kokola", function () {
        document.addEventListener('deviceready', function () {

            //downloadFile(urliki, nama_file);
            window.open = cordova.InAppBrowser.open;
            window.open(encodeURI(urliki), '_system', 'location=no');
        }, false);
    });
});
function downloadFile(urlaa, namafile) {

    //var downloadUrl = urlaa;
    var downloadUrl = encodeURI(urlaa);
    var fileName = namafile;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
        var fileTransfer = new FileTransfer();
        fileTransfer.download(
                downloadUrl,
                cordova.file.externalApplicationStorageDirectory + fileName,
                //fileSystem.root.toURL() + '/' + fileName,
                        function (entry) {
                            alert("download complete: " + entry.fullPath);
                        },
                        function (error) {
                            alert("download error source " + error.source);
                            alert("download error target " + error.target);
                            alert("download error code" + error.code);
                        },
                        false,
                        {
                            headers: {
                                "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                            }
                        }
                );
            });
}


/* ===== Modals end Page upload_pic  ===== */


/* ===== Modals Page baca_pic  ===== */
myApp.onPageInit('baca_pic', function (page) {
    popup_sts = 0;
    lokasi_sekrng = "baca_pic";
    //alert(page.query.id_subtopic);
    $$(".id_subtopic_pic").val(page.query.id_subtopic);
    $$.ajax({
        url: ipserver + 'pic/act_pic.php',
        method: "POST",
        data: {op: "baca_data_upic", id_sub: page.query.id_subtopic},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {
            $$(".txt_subtopic").html(kiriman[8]);
            $$(".txt_nama_lead").text(kiriman[0]);
            $$(".txt_tgl_meet").text(kiriman[1]);
            $$(".txt_waktu_meet").text(kiriman[2]);
            $$(".txt_tempat_meet").text(kiriman[3]);
            $$(".txt_topic_meet").text(kiriman[4]);
            $$(".txt_tools_meet").text(kiriman[5]);
            $$(".txt_nama_pic").text(kiriman[6]);
            $$(".txt_ddate").text(kiriman[7]);
            if (kiriman[9] != "" && kiriman[9] != null) {
                $$("#catatan").val(kiriman[9]).trigger("change");
            }
            if (kiriman[10] == "") {
                $$(".kategori").text("-");
            } else {
                $$(".kategori").text(kiriman[10]);
            }
        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
    baca_list_upload_saja(page.query.id_subtopic);
    $$("#b_back").on("click", function () {
        mainView.router.loadPage("pic.html");
    });
});
/* ===== Modals end Page baca_pic  ===== */

/* ===== Modals Page cek_lead  ===== */
myApp.onPageInit('cek_lead', function (page) {
    popup_sts = 0;
    lokasi_sekrng = "cek_lead";
    //alert(page.query.id_sub);
    $$.ajax({
        url: ipserver + 'pic/act_pic.php',
        method: "POST",
        data: {op: "baca_data_upic", id_sub: page.query.id_sub},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {
            $$(".txt_subtopic").html(kiriman[8]);
            $$(".txt_nama_lead").text(kiriman[0]);
            $$(".txt_tgl_meet").text(kiriman[1]);
            $$(".txt_waktu_meet").text(kiriman[2]);
            $$(".txt_tempat_meet").text(kiriman[3]);
            $$(".txt_topic_meet").text(kiriman[4]);
            $$(".txt_tools_meet").text(kiriman[5]);
            $$(".txt_nama_pic").text(kiriman[6]);
            $$(".txt_ddate").text(kiriman[7]);
            if (kiriman[9] != "" && kiriman[9] != null) {
                $$("#catatan").val(kiriman[9]).trigger("change");
            }

            if (kiriman[10] == "") {
                $$(".kategori").text("-");
            } else {
                $$(".kategori").text(kiriman[10]);
            }

        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
    $$.ajax({
        url: ipserver + 'lead/act_lead.php',
        method: "POST",
        data: {op: "baca_data", id_sub: page.query.id_sub},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {

            if (kiriman[0] != '1') {
                $$("#b_revisi").hide();
                $$("#b_approve").hide();
            }

            if (kiriman[0] == '1') {
                $$("#div_upload_lead_rev").hide();
                $$("#judul_cek_lead").text("Lead (Done)");
            }
            else if (kiriman[0] == '2') {
                $$("#judul_cek_lead").text("Lead (Revised)");
                $$("#div_file_lead").hide();
            }
            else if (kiriman[0] == '3') {
                $$("#judul_cek_lead").text("Lead (Closed)");
                $$("#div_file_lead").hide();
            }


            if (kiriman[1] != '' && kiriman[1] != null) {
                $$("#catatan_revisi").val(kiriman[1]).trigger("change");
            }

        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });


    baca_list_upload_saja(page.query.id_sub);
    baca_list_uploadrevisi_saja(page.query.id_sub);

    $.validator.addMethod('filesize', function (value, element, param) {

        return this.optional(element) || (element.files[0].size <= param)
    });
    //validate login
    $("#f_cek_lead").validate({
        rules: {
            catatan_revisi: {
                required: true
            },
            u_lead: {
                filesize: (1024 * 1024) * 2
            }
        },
        messages: {
            u_lead: {
                filesize: "File must be less than 2MB"
            }
        }
    });
//end validate login

    $$("#b_revisi").on("click", function () {
        if ($("#f_cek_lead").valid()) {
            myApp.confirm("Are you sure?", "Kokola", function () {
                //alert("on clik revisi");
                $$(".id_subtopic_lead").val(page.query.id_sub);
                var form = $$("#f_cek_lead")[0];
                var formData = new FormData(form);

                // alert(JSON.stringify(formData));
                // return false;


                $$.ajax({
                    url: ipserver + 'lead/act_lead.php',
                    method: "POST",
                    //data: {op: "revisi", id_sub: page.query.id_sub, note_lead: $$("#catatan_revisi").val()},
                    data: formData,
                    dataType: "json",
                    timeout: 3 * 60 * 1000,
                    beforeSend: function () {
                        myApp.showIndicator();
                    },
                    success: function (kiriman) {
                        if (kiriman == "ok") {
                            myApp.alert("Save Success ", "Kokola", function () {
                                mainView.router.loadPage('lead.html');
                            });
                        } else {
                            myApp.alert("Failed", "Kokola");
                        }

                    },
                    error: function (data) {
                        myApp.alert('No Connection or Error', 'Kokola');
                        myApp.hideIndicator();
                    },
                    complete: function () {
                        myApp.hideIndicator();
                    }
                });
            });
        }
    });
    $$("#b_approve").on("click", function () {
        myApp.confirm("Are you sure?", "Kokola", function () {
            $$.ajax({
                url: ipserver + 'lead/act_lead.php',
                method: "POST",
                data: {op: "approve", id_sub: page.query.id_sub, note_lead: $$("#catatan_revisi").val()},
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "ok") {
                        myApp.alert("Save Success ", "Kokola", function () {
                            mainView.router.loadPage('lead.html');
                        });
                    } else {
                        myApp.alert("Failed", "Kokola");
                    }

                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        });
    });
    $$("#b_back").on("click", function () {
        mainView.router.loadPage('lead.html');
    });
});
/* ===== Modals Page end cek_lead  ===== */

/* ===== Modals Page upload_pic_revisi  ===== */
myApp.onPageInit('upload_pic_revisi', function (page) {
    popup_sts = 0;
    lokasi_sekrng = "upload_pic_revisi";
    // alert(page.query.id_subtopic);
    $$(".id_subtopic_pic").val(page.query.id_subtopic);

    $$.ajax({
        url: ipserver + 'pic/act_pic.php',
        method: "POST",
        data: {op: "baca_data_upic", id_sub: page.query.id_subtopic},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {
            $$(".txt_subtopic").html(kiriman[8]);
            $$(".txt_nama_lead").text(kiriman[0]);
            $$(".txt_tgl_meet").text(kiriman[1]);
            $$(".txt_waktu_meet").text(kiriman[2]);
            $$(".txt_tempat_meet").text(kiriman[3]);
            $$(".txt_topic_meet").text(kiriman[4]);
            $$(".txt_tools_meet").text(kiriman[5]);
            $$(".txt_nama_pic").text(kiriman[6]);
            $$(".txt_ddate").text(kiriman[7]);
            if (kiriman[9] != "" && kiriman[9] != null) {
                $$("#catatan").val(kiriman[9]).trigger("change");
            }

            if (kiriman[10] == "") {
                $$(".kategori").text("-");
            } else {
                $$(".kategori").text(kiriman[10]);
            }
        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });

    $$.ajax({
        url: ipserver + 'lead/act_lead.php',
        method: "POST",
        data: {op: "baca_data", id_sub: page.query.id_subtopic},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (kiriman) {


            if (kiriman[1] != '' && kiriman[1] != null) {
                $$("#catatan_revisi").val(kiriman[1]).trigger("change");
            }

        },
        error: function (data) {
            myApp.alert('No Connection or Error', 'Kokola');
            myApp.hideIndicator();
        },
        complete: function () {
            myApp.hideIndicator();
        }
    });
    $.validator.addMethod('filesize', function (value, element, param) {

        return this.optional(element) || (element.files[0].size <= param)
    });
    $("#f_upload_pic_revisi").validate({
        rules: {
            u_pic: {
                required: true,
                filesize: (1024 * 1024) * 2
            }

        },
        messages: {
            u_pic: {
                filesize: "File must be less than 2MB"
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parents(".up_pic"));
        }
    });
    $$("#u_pic").on("change", function () {
        // alert("masukkk berubah revisi");
        if ($("#f_upload_pic_revisi").valid()) {
            //alert("tes");
            var form = $$("#f_upload_pic_revisi")[0];
            var formData = new FormData(form);
            $$.ajax({
                url: ipserver + 'pic/act_pic.php',
                method: "POST",
                data: formData,
                dataType: "json",
                timeout: 10 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "sukses") {
                        $$("#f_upload_pic_revisi")[0].reset();
                        baca_list_upload(page.query.id_subtopic);
                        myApp.alert("Upload Succes", "Kokola");
                    } else {
                        myApp.alert("Upload Failed", "Kokola");
                    }
                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        }


    });
    $$("#b_simpan_upload_pic").on("click", function () {
        //alert("save");
        myApp.confirm("Are you sure want to Finish?", "Kokola", function () {
            $$.ajax({
                url: ipserver + 'pic/act_pic.php',
                method: "POST",
                data: {op: "finish_pic_revisi", id_sub: page.query.id_subtopic, note: $$("#catatan").val()},
                dataType: "json",
                timeout: 3 * 60 * 1000,
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (kiriman) {
                    if (kiriman == "ok") {
                        myApp.alert("Save Success", "Kokola");
                        mainView.router.loadPage("pic.html");
                    } else {
                        myApp.alert("Failed", "Kokola");
                    }

                },
                error: function (data) {
                    myApp.alert('No Connection or Error', 'Kokola');
                    myApp.hideIndicator();
                },
                complete: function () {
                    myApp.hideIndicator();
                }
            });
        });
    });

    baca_list_upload(page.query.id_subtopic);
    baca_list_uploadrevisi_saja(page.query.id_subtopic);

    $$("#b_back").on("click", function () {
        mainView.router.loadPage("pic.html");
    });
});
/* ===== Modals Page end upload_pic_revisi  ===== */


/* ===== Modals Page kaizen  ===== */
myApp.onPageInit('kaizen', function (page) {
    awal = 1;
    popup_sts = 0;
    lokasi_sekrng = "kaizen";

    //alert(lokasi_sekrng);
    load_semua();
    function load_semua() {
        // alert("tess besok");
        var data_post = {id_pic: ls_id, op: "repair"};

        $$.ajax({
            url: ipserver + 'kaizen/repair.php',
            method: "POST",
            data: data_post,
            dataType: "json",
            timeout: 5 * 60 * 1000,
            beforeSend: function () {
                //myApp.showIndicator();
            },
            success: function (kiriman) {
                var isinya = '';
                //   if (a == "pertama") {
                if (kiriman.cek_ada == "ada") {
                    var tab1='<div class="timeline">';
                    
                    for (var i = 0; i < kiriman.group.length; i++) {
                        //nama tgl grouop
                        tab1+=' <div class="timeline-item">'
                                +'<div class="timeline-item-date"><small>'+kiriman.group[i]+'</small></div>'
                                +'<div class="timeline-item-divider"></div>'
                                +'<div class="timeline-item-content">';
                        for (var j = 0; j < kiriman.group_data[kiriman.group[i]].count; j++) {
                            //nama itemnya
                            tab1+='<div class="timeline-item-inner">'
                                        +'<div class="timeline-item-time">Lead : '+kiriman.group_data[kiriman.group[i]].items[j][3]
                                        +' <b class="color-green">(Done)</b></div>'
                                     +kiriman.group_data[kiriman.group[i]].items[j][1]   
                                    +'</div>';
                        }
                        
                        tab1+='</div>'
                            +'</div>';
                    }
                    
                    tab1+='</div>';
                    
                    $$("#div_kaizen_tab1").html(tab1);
                } else {

                    $$("#div_kaizen_tab1").html('<li class="list-group-title">Data Not Found</li>');
                   
                }

            },
            error: function (data) {
                myApp.alert('No Connection or Error', 'Kokola');
                //myApp.pullToRefreshDone();
            },
            complete: function () {
               //myApp.hideIndicator();
               // myApp.pullToRefreshDone();
            }
        });
    }


});
/* ===== Modals end Page kaizen  ===== */

$$(document).on("click", ".peringatan", function () {

    myApp.alert("*NB: File secara otomatis akan dihapus setelah 3 bulan.", "Kokola");

});

$$(document).on("click", ".dept", function () {

    document.addEventListener("deviceready", getar, false);
    function getar() {
        navigator.vibrate([500, 500, 500]);
        navigator.notification.beep(1);
    }

    myApp.alert("E-Apps Department", "Kokola");

});


function terima_notif() {
    $$.ajax({
        url: ipserver + 'meeting/terima_notif_undangan.php',
        method: "POST",
        data: {op: "undangan", id_p: localStorage.getItem("ls_id")},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            // myApp.showIndicator();
        },
        success: function (kiriman) {
            if (kiriman == "ok") {
                //myApp.alert("Terima Notif", "Kokola");
            } else {
                //myApp.alert("Failed", "Kokola");
            }

        },
        error: function (data) {
            //myApp.alert('No Connection or Error', 'Kokola');
            // myApp.hideIndicator();
        },
        complete: function () {
            // myApp.hideIndicator();
        }
    });

}


var versi_em = "belum";
function cek_versi_baru(versi) {

    $$.ajax({
        url: ipserver + 'cek_versi.php',
        method: "POST",
        data: {op: "cek", v_apk: versi},
        dataType: "json",
        timeout: 3 * 60 * 1000,
        beforeSend: function () {
            //  myApp.showIndicator();
        },
        success: function (kiriman) {

            if (kiriman[0] == "old") {

                function onConfirm(buttonIndex) {
                    versi_em = "belum";
                    window.open = cordova.InAppBrowser.open;
                    window.open(encodeURI(kiriman[1]), '_system', 'location=no');
                }
                if (versi_em == "belum") {

                    versi_em = "sudah";
                    navigator.notification.confirm(
                            kiriman[2], // message
                            onConfirm, // callback to invoke with index of button pressed
                            'Kokola E-Meeting', // title
                            ['OK']     // buttonLabels
                            );
                }
            }


        },
        error: function () {
            //myApp.alert('No Connection or Error', 'Kokola');
            //myApp.hideIndicator();
        },
        complete: function () {
            // myApp.hideIndicator();
        }
    });

}


//interval
setInterval(function () {
    //alert(ls_id +" "+ localStorage.getItem("ls_id"));
    if (ls_id == "" || ls_id == null ||
            localStorage.getItem("ls_id") == "" || localStorage.getItem("ls_id") == null) {
        myApp.loginScreen();
        localStorage.clear();
        ls_id = "";
        ls_nama = "";
        ls_email = "";
        ls_pass = "";
    }
    if (lokasi_sekrng == "index") {
        load_div_this_month("auto");
        load_div_month_besok("auto");
        //load_div_riwayat("auto");

        cek_versi_baru("V1.11");

    } else if (lokasi_sekrng == "edit_notulen") {
        load_sub_topic("auto");
        cek_peserta_terkirim();
        cek_cc_peserta();
    } else if (lokasi_sekrng == "pic") {
        load_pic_tab1("auto");
        load_pic_tab2("auto");
        load_pic_tab3("auto");
    }
    else if (lokasi_sekrng == "lead") {
        load_lead_tab1("auto")
        load_lead_tab2("auto")
        load_lead_tab3("auto")
    }
}, 9000);


