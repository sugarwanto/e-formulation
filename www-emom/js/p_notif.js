/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        console.log('Received Device Ready Event');
        console.log('calling setup push');
        app.setupPush();
    },
    setupPush: function () {
        console.log('calling push init');
        var push = PushNotification.init({
            "android": {
                "senderID": "892919177992"
            },
            "ios": {
                "sound": true,
                "vibration": true,
                "badge": true
            },
            "windows": {}
        });
        console.log('after init');

        push.on('registration', function (data) {
            console.log('registration event: ' + data.registrationId);

            var oldRegId = localStorage.getItem('registrationId');
            if (oldRegId !== data.registrationId) {
                // Save new registration ID
                localStorage.setItem('registrationId', data.registrationId);

                // Post registrationId to your app server as the value has changed
            }
            //alert(data.registrationId);

            document.getElementById('api_key').value = data.registrationId;

            //localStorage.setItem('id_eps', apikey);
            //alert("API nya "+apikey);

        });

        push.on('error', function (e) {
            console.log("push error = " + e.message);
        });

        push.on('notification', function (data) {

            var navigasi = data.additionalData.ke_hal;
            var tanggal = data.additionalData.tanggal;

            myApp.addNotification({
                message: '<span class="color-yellow">' + data.title + '</span><br/>'
                        + data.message + '<br/>'
                        + '<span class="color-lightgreen">' + tanggal + '</span>',
                hold: 10000,
                closeOnClick: true,
                onClick: function () {
                    if (navigasi == "home") {                      
                        if (awal != 0) {                           
                            mainView.router.loadPage("index.html");
                        }
                    } else if (navigasi == "lead") {
                        mainView.router.loadPage("lead.html");
                    } else if (navigasi == "pic") {
                        mainView.router.loadPage("pic.html");
                    }
                    myApp.closePanel();
                }
            });

            //getar
            navigator.vibrate([500, 500, 500]);
            navigator.notification.beep(1);
            
            //terima notif
            if(navigasi == "home"){
                  terima_notif();
            }
            
        });
    }
//    btn_kirim: function () {
//        terima_notif();
//    }
};
